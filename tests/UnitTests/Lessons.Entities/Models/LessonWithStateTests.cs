using System;
using FluentAssertions;
using Lessons.Entities.Enums;
using Lessons.Entities.Exceptions;
using Lessons.Entities.Models;
using Xunit;

namespace UnitTests.Lessons.Entities.Models
{
    public class LessonWithStateTests
    {
        #region CreateUnboundLesson

        [Fact]
        public void CreateUnboundLesson_AfterCreate_StatusIsOpened()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.Status.Should().Be(LessonStatus.OpenedForSubscribe);
        }

        [Fact]
        public void CreateUnboundLesson_AfterCreate_ByRequestIsFalse()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.ByRequest.Should().Be(false);
        }

        [Fact]
        public void CreateUnboundLesson_AfterCreate_CheckStatusIsFalse()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.CheckStatus().Should().Be(false);
        }

        #endregion

        #region SubscribeStudent

        [Fact]
        public void SubscribeStudent_Subscribe2Students_StudentCountIs2()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            lessson.StudentsCount.Should().Be(2);
        }

        [Fact]
        public void SubscribeStudent_Subscribe2Students_StatusIsPending()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            lessson.Status.Should().Be(LessonStatus.Pending);
        }

        [Fact]
        public void SubscribeStudent_Subscribe3Students_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<Student> act = () => lessson.SubscribeStudent(Guid.NewGuid());

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void SubscribeStudent_LessonIsCanceled_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.CancelLesson(LessonBuilder.TeacherId, "");

            Func<Student> act = () => lessson.SubscribeStudent(Guid.NewGuid());

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void SubscribeStudent_SubscribeOneStudentTwice_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(LessonBuilder.StudentId);

            Func<Student> act = () => lessson.SubscribeStudent(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region UnSubscribeStudent

        [Fact]
        public void UnSubscribeStudent_UnSubscribe1Student_StudentCountIs1()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());

            lessson.SubscribeStudent(LessonBuilder.StudentId);
            lessson.UnsubscribeStudent(LessonBuilder.StudentId);

            lessson.StudentsCount.Should().Be(1);
        }

        [Fact]
        public void UnSubscribeStudent_UnSubscribe1Students_StatusIsOpen()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());

            lessson.SubscribeStudent(LessonBuilder.StudentId);
            lessson.UnsubscribeStudent(LessonBuilder.StudentId);

            lessson.Status.Should().Be(LessonStatus.OpenedForSubscribe);
        }

        [Fact]
        public void UbnsubscribeStudent_LessonIsCanceled_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(LessonBuilder.StudentId);
            lessson.CancelLesson(LessonBuilder.TeacherId, "");

            Func<Student> act = () => lessson.UnsubscribeStudent(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void UbnsubscribeStudent_StudentCountIs0_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<Student> act = () => lessson.UnsubscribeStudent(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void UbnsubscribeStudent_StudentIsUnsubscribed_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<Student> act = () => lessson.UnsubscribeStudent(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region JoinLesson

        [Fact]
        public void JoinLesson_SubscribedStudent_InviteLinkIsInvite()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(LessonBuilder.StudentId);
            lessson.StartLesson(LessonBuilder.TeacherId, LessonBuilder.LinkText);

            var link = lessson.JoinLesson(LessonBuilder.StudentId);

            link.Should().Be(LessonBuilder.LinkText);
        }

        [Fact]
        public void JoinLesson_SubscribedStudent_IsAttendanedIsTrue()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            var student = lessson.SubscribeStudent(LessonBuilder.StudentId);
            lessson.StartLesson(LessonBuilder.TeacherId, LessonBuilder.LinkText);

            var link = lessson.JoinLesson(LessonBuilder.StudentId);

            student.IsAttendaned.Should().Be(true);
        }

        [Fact]
        public void JoinLesson_StudentIsUnsubscribed_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<string> act = () => lessson.JoinLesson(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void JoinLesson_LessonIsNotStarted_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.SubscribeStudent(LessonBuilder.StudentId);

            Func<string> act = () => lessson.JoinLesson(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region GetInviteLink

        [Fact]
        public void GetInviteLink_AttendanedStudent_InviteLinkIsInvite()
        {
            var lessson = LessonBuilder.StartLesson();
            lessson.JoinLesson(LessonBuilder.StudentId);

            var link = lessson.GetInviteLink(LessonBuilder.StudentId);

            link.Should().Be(LessonBuilder.LinkText);
        }

        [Fact]
        public void GetInviteLink_ToTeacher_InviteLinkIsInvite()
        {
            var lessson = LessonBuilder.StartLesson();

            var link = lessson.GetInviteLink(LessonBuilder.TeacherId);

            link.Should().Be(LessonBuilder.LinkText);
        }

        [Fact]
        public void GetInviteLink_StudentIsNotAttendaned_ThrowLessonException()
        {
            var lessson = LessonBuilder.StartLesson();

            Func<string> act = () => lessson.GetInviteLink(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void GetInviteLink_LessonIsNotStarted_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(LessonBuilder.StudentId);

            Func<string> act = () => lessson.GetInviteLink(LessonBuilder.StudentId);

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region SetMaxStudentCount

        [Fact]
        public void SetMaxStudentCount_AfterSubscribe2StudentsSetTo1_ThrowLessonParameterException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<bool> act = () => lessson.SetMaxStudentCount(LessonBuilder.TeacherId, 1);

            act.Should().Throw<LessonParameterException>();
        }

        [Fact]
        public void SetMaxStudentCount_SetTo3ByNoAuther_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<bool> act = () => lessson.SetMaxStudentCount(Guid.NewGuid(), 3);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void SetMaxStudentCount_SetTo3ByAutherLessonIsCanceled_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.CancelLesson(LessonBuilder.TeacherId, "");

            Func<bool> act = () => lessson.SetMaxStudentCount(LessonBuilder.TeacherId, 3);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void SetMaxStudentCount_SetTo1MinStudentCountIs2_ThrowLessonParameterException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<bool> act = () => lessson.SetMaxStudentCount(LessonBuilder.TeacherId, 1);

            act.Should().Throw<LessonParameterException>();
        }

        [Fact]
        public void SetMaxStudentCount_SetTo3_StatusIsOpened()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.SetMaxStudentCount(LessonBuilder.TeacherId, 3);

            lessson.Status.Should().Be(LessonStatus.OpenedForSubscribe);
        }

        #endregion

        #region EditLesson

        [Fact]
        public void EditLesson_EditmaxStudentCount_maxStudentCountIs4()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            lessson.EditLesson(LessonBuilder.TeacherId, "sss",
                "ddd", DateTime.Now, 50, 4, 1, 100);

            lessson.MaxStudentCount.Should().Be(4);
        }

        [Fact]
        public void EditLesson_ByNoAuther_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<bool> act = () => lessson.EditLesson(Guid.NewGuid(), "sss",
                "ddd", DateTime.Now, 50, 4, 1, 100);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void EditLesson_LessonIsCanceled_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.CancelLesson(LessonBuilder.TeacherId, "");

            Func<bool> act = () => lessson.EditLesson(LessonBuilder.TeacherId, "sss",
                "ddd", DateTime.Now, 50, 4, 1, 100);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void EditLesson_SetTo1MinStudentCountIs2_ThrowLessonParameterException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<bool> act = () => lessson.EditLesson(LessonBuilder.TeacherId, "sss",
                "ddd", DateTime.Now, 50, 1, 2, 100);

            act.Should().Throw<LessonParameterException>();
        }

        [Fact]
        public void EditLesson_AfterSubscribe2StudentsSetTo1_ThrowLessonParameterException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<bool> act = () => lessson.EditLesson(LessonBuilder.TeacherId, "sss",
                "ddd", DateTime.Now, 50, 1, 1, 100);

            act.Should().Throw<LessonParameterException>();
        }

        #endregion

        #region StartLesson

        [Fact]
        public void StartLesson_AfterStart_InviteLinkIsInvite()
        {
            var lessson = LessonBuilder.StartLesson();

            var link = lessson.GetInviteLink(LessonBuilder.TeacherId);

            link.Should().Be(LessonBuilder.LinkText);
        }

        [Fact]
        public void StartLesson_AfterStart_StatusIsStarted()
        {
            var lessson = LessonBuilder.StartLesson();

            lessson.Status.Should().Be(LessonStatus.Started);
        }

        [Fact]
        public void StartLesson_LessonIsCanceled_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.CancelLesson(LessonBuilder.TeacherId, "");

            Func<bool> act = () => lessson.StartLesson(LessonBuilder.TeacherId, LessonBuilder.LinkText);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void StartLesson_ByNoAuther_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<bool> act = () => lessson.StartLesson(Guid.NewGuid(), LessonBuilder.LinkText);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void StartLesson_StudentCountLessMinStudentCount_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());

            Func<bool> act = () => lessson.StartLesson(LessonBuilder.TeacherId, LessonBuilder.LinkText);

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region StopLesson

        [Fact]
        public void StopLesson_AfterStop_StatusIsCompleted()
        {
            var lessson = LessonBuilder.StartLesson();
            lessson.StopLesson(LessonBuilder.TeacherId);

            lessson.Status.Should().Be(LessonStatus.Completed);
        }

        [Fact]
        public void StopLesson_LessonIsNotStarted_ThrowLessonException()
        {
            var lessson = LessonBuilder.CreateLesson();

            Func<bool> act = () => lessson.StopLesson(LessonBuilder.TeacherId);

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void StopLesson_ByNoAuther_ThrowLessonException()
        {
            var lessson = LessonBuilder.StartLesson();

            Func<bool> act = () => lessson.StopLesson(Guid.NewGuid());

            act.Should().Throw<LessonException>();
        }

        #endregion

        #region CancelLesson

        [Fact]
        public void CancelLesson_AfterCancel_StatusIsCanceled()
        {
            var lessson = LessonBuilder.CreateLesson();

            lessson.CancelLesson(LessonBuilder.TeacherId, "efefref");

            lessson.Status.Should().Be(LessonStatus.Canceled);
        }

        [Fact]
        public void CancelLesson_LessonIsStarted_ThrowLessonException()
        {
            var lessson = LessonBuilder.StartLesson();

            Func<bool> act = () => lessson.CancelLesson(LessonBuilder.TeacherId, "eee");

            act.Should().Throw<LessonException>();
        }

        [Fact]
        public void CancelLesson_ByNoAuther_ThrowLessonException()
        {
            var lessson = LessonBuilder.StartLesson();

            Func<bool> act = () => lessson.CancelLesson(Guid.NewGuid(), "sss");

            act.Should().Throw<LessonException>();
        }

        #endregion
    }
}