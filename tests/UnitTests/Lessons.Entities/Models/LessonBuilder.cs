using System;
using Lessons.Entities.Models;

namespace UnitTests.Lessons.Entities.Models
{
    public static class LessonBuilder
    {
        public static Guid TeacherId = Guid.NewGuid();
        public static Guid StudentId = Guid.NewGuid();
        public static string LinkText = "Invite";

        public static LessonWithState CreateLesson()
        {
            var lesson = LessonWithState.CreateUnboundLesson(
                TeacherId,
                "sss",
                DateTime.Now.AddDays(5),
                maxStudentCount: 2,
                minStudentCount: 2
            );
            return lesson;
        }

        public static LessonWithState StartLesson()
        {
            var lessson = CreateLesson();
            lessson.SubscribeStudent(Guid.NewGuid());
            lessson.SubscribeStudent(StudentId);
            lessson.StartLesson(TeacherId, LinkText);
            return lessson;
        }
    }
}