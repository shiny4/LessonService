using System.ComponentModel.DataAnnotations;

namespace Lessons.Entities.Enums
{
    public enum LessonStatus
    {
        OpenedForSubscribe, //открыто для записи студентов
        Pending, // запись закрыта, ожидает начала
        Started, // начато
        Completed, // завершено
        Canceled, //отменено
        Expired // просрочен, т.е. истек срок проведения урока
    }
}