using System;

namespace Lessons.Entities.Exceptions
{
    public class LessonParameterException : Exception
    {
        public string PropertyName { get; private set; }
        public string ErrorMessage { get; private set; }

        public LessonParameterException(string message, string propertyName, string errorMessage)
            : base(message)
        {
            PropertyName = propertyName;
            ErrorMessage = errorMessage;
        }
    }
}