using System;

namespace Lessons.Entities.Models
{
    public class Email : BaseEntity
    {
        public Guid LessonWithStateIdId { get; set; }
        public DateTime StartDateTime { get; set; }
        public string Address { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsSended { get; set; } = false;
        public bool IsRejected { get; set; } = false;
        public int Attempts { get; set; } = 0;

        public int Category { get; set; } = 0;
    }
}