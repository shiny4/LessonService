using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lessons.Entities.Enums;
using Lessons.Entities.Exceptions;

namespace Lessons.Entities.Models
{
    public class LessonWithState : LessonBase
    {
        protected LessonWithState(string lessonTitle, Guid teacherId, DateTime startDate) :
            base(lessonTitle, teacherId, startDate)
        {
        }

        protected LessonWithState(Guid guid, string lessonTitle, Guid teacherId, DateTime startDate)
            : base(guid, lessonTitle, teacherId, startDate)
        {
        }

        #region управление состоянием урока и манипуляции, приводящие к изменению его состояния

        public bool IsLessonCanBeModified()
        {
            if (_status == LessonStatus.Canceled ||
                _status == LessonStatus.Completed ||
                _status == LessonStatus.Expired ||
                _status == LessonStatus.Started)
                return false;

            return true;
        }

        /// <summary>
        /// Проверка корректности состояния
        /// </summary>
        /// <returns>true - если состояние изменилось после проверки</returns>
        public bool CheckStatus()
        {
            switch (_status)
            {
                case LessonStatus.Canceled:
                case LessonStatus.Completed:
                    return false;

                case LessonStatus.Started:
                    if (DateTime.Now >= EndDate)
                    {
                        _status = LessonStatus.Completed;
                        return true;
                    }

                    if (_maxStudentCount > 0 && _maxStudentCount < Students.Count) _maxStudentCount = Students.Count;
                    return false;

                case LessonStatus.Expired:
                    if (DateTime.Now < _startDate)
                    {
                        if (_maxStudentCount == 0 || _maxStudentCount > Students.Count)
                            _status = LessonStatus.OpenedForSubscribe;
                        else
                            _status = LessonStatus.Pending;
                        return true;
                    }

                    return false;

                case LessonStatus.OpenedForSubscribe:
                    if (DateTime.Now > _startDate)
                    {
                        _status = LessonStatus.Expired;
                        return true;
                    }

                    if (_maxStudentCount == 0 || Students.Count == 0 || _maxStudentCount > Students.Count) return false;
                    _status = LessonStatus.Pending;
                    return true;

                case LessonStatus.Pending:
                    if (DateTime.Now > _startDate)
                    {
                        _status = LessonStatus.Expired;
                        return true;
                    }

                    if (_maxStudentCount > 0 && Students.Count >= _maxStudentCount) return false;
                    _status = LessonStatus.OpenedForSubscribe;
                    return true;
            }

            return false;
        }

        /// <summary>
        /// запуск урока
        /// </summary>
        /// <param name="initiatorId"></param>
        /// <param name="inviteLink">ссылка на канал видеосвязи</param>
        /// <returns>true - если состояние урока изменилось</returns>
        /// <exception cref="LessonException"></exception>
        public bool StartLesson(Guid initiatorId, string inviteLink = "")
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Начать урок может только преподаватель с id={TeacherId}");

            switch (Status)
            {
                case LessonStatus.Started:
                    _inviteLink = inviteLink;
                    return false;
                case LessonStatus.Expired:
                    throw new LessonException(
                        $"Срок проведения занятия истек {_startDate.ToString(CultureInfo.CurrentCulture)}");
                case LessonStatus.Canceled:
                case LessonStatus.Completed:
                    throw new LessonException(
                        $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть запущен.");
            }

            if (StudentsCount < MinStudentCount) //занятие нельзя начать, записавшихся учеников слишком мало
                throw new LessonException(
                    $"Количество записанных учеников на урок с id='{Id}'  слишком мало. Занятие нельзя начать.");

            _inviteLink = inviteLink;
            _status = LessonStatus.Started;
            return true;
        }

        public bool StopLesson(Guid initiatorId)
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Закончить урок может только преподаватель с id={TeacherId}");

            switch (Status)
            {
                case LessonStatus.Started:
                    _status = LessonStatus.Completed;
                    return true;
                case LessonStatus.Expired:
                    throw new LessonException(
                        $"Срок проведения занятия истек {_startDate.ToString(CultureInfo.CurrentCulture)}");
                default:
                    throw new LessonException(
                        $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть остановлен.");
            }
        }

        /// <summary>
        /// Отмена урока
        /// </summary>
        /// <param name="initiatorId"></param>
        /// <param name="reason"></param>
        /// <returns>true - если состояние урока изменилось</returns>
        /// <exception cref="StudentNotAllowedActionException"></exception>
        /// <exception cref="LessonCanNotBeCanceledException"></exception>
        public bool CancelLesson(Guid initiatorId, string reason)
        {
            if (initiatorId != TeacherId)
                throw new LessonException(
                    $"Отменить урок может только преподаватель с id={TeacherId}");

            switch (Status)
            {
                case LessonStatus.Canceled:
                    return false;
                case LessonStatus.Completed:
                case LessonStatus.Started:
                    throw new LessonException(
                        $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть отменен.");
            }

            return CancelLessonBase(initiatorId, reason);
        }

        private bool CancelLessonBase(Guid initiatorId, string reason)
        {
            var c = new CanceledLesson()
            {
                CancelDate = DateTime.Now,
                InitiatorId = initiatorId,
                Lesson = this,
                Reason = reason,
                LessonId = Id
            };
            СanceledLesson = c;
            _status = LessonStatus.Canceled;
            return true;
        }

        private void InitValues(string lessonTitle, string lessonDescription,
            DateTime startDate, int durationInMin,
            int maxStudentCount, int minStudentCount, decimal price)
        {
            _lessonTitle = lessonTitle;
            _lessonDescription = lessonDescription;
            _price = price;
            _startDate = startDate;
            _durationInMin = durationInMin;

            if (maxStudentCount > 0 && maxStudentCount < minStudentCount) //максимальное количество студентов не может
                //быть установлено меньше, чем минимальное число для начала занятий
                throw new LessonParameterException("Validation failed for lesson init", nameof(maxStudentCount),
                    $"maxStudentCount({{maxStudentCount}}) must be greater or equal than minStudentCount({minStudentCount})");
            //maxStudentCount = minStudentCount;

            _minStudentCount = minStudentCount;

            //максимальное количество студентов не может быть установлено меньше, чем уже записано
            if (maxStudentCount > 0 && maxStudentCount < StudentsCount)
                //_maxStudentCount = StudentsCount;
                throw new LessonParameterException("Validation failed for lesson init", nameof(maxStudentCount),
                    $"maxStudentCount({maxStudentCount}) must be greater or equal than StudentsCount ({StudentsCount})");
            else
                _maxStudentCount = maxStudentCount;
        }

        public bool EditLesson(Guid initiatorId, string lessonTitle, string lessonDescription,
            DateTime startDate, int durationInMin,
            int maxStudentCount, int minStudentCount, decimal price)
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Редактировать урок может только преподаватель с id={TeacherId}");

            if (!IsLessonCanBeModified())
                throw new LessonException(
                    $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть изменен.");

            InitValues(lessonTitle, lessonDescription, startDate, durationInMin, maxStudentCount, minStudentCount,
                price);
            return CheckStatus();
        }

        public static LessonWithState CreateUnboundLesson(Guid teacherId, string lessonTitle,
            DateTime startDate, int durationInMin = 60, string lessonDescription = "",
            int maxStudentCount = 0, int minStudentCount = 1, decimal price = 100,
            Guid id = default, Guid parentRequestId = default)
        {
            var lesson = id == default
                ? new LessonWithState(lessonTitle, teacherId, startDate)
                : new LessonWithState(id, lessonTitle, teacherId, startDate);
            if (parentRequestId != Guid.Empty) lesson._parentRequestId = parentRequestId;

            lesson.InitValues(lessonTitle, lessonDescription, startDate, durationInMin,
                maxStudentCount, minStudentCount, price);

            return lesson;
        }

        public bool MoveLessonTime(Guid initiatorId, DateTime startDate, int durationInMin)
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Редактировать урок может только преподаватель с id={TeacherId}");

            if (!IsLessonCanBeModified())
                throw new LessonException(
                    $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть изменен.");
            _startDate = startDate;
            _durationInMin = durationInMin;
            return CheckStatus();
        }

        public bool MoveLessonEnd(Guid initiatorId, int durationInMin)
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Редактировать урок может только преподаватель с id={TeacherId}");

            if (!IsLessonCanBeModified())
                throw new LessonException(
                    $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть изменен.");
            _durationInMin = durationInMin;
            return CheckStatus();
        }

        public bool SetMaxStudentCount(Guid initiatorId, int maxStudentCount)
        {
            if (initiatorId != TeacherId)
                throw new LessonException($"Редактировать урок может только преподаватель с id={TeacherId}");

            if (!IsLessonCanBeModified())
                throw new LessonException(
                    $"Урок имеет статус '{_status.ToString()}'.  Урок не может быть изменен.");

            if (maxStudentCount > 0 && maxStudentCount < _minStudentCount) //максимальное количество студентов не может
                //быть установлено меньше, чем минимальное число для начала занятий
                throw new LessonParameterException("Validation failed for lesson init", nameof(maxStudentCount),
                    $"maxStudentCount({{maxStudentCount}}) must be greater or equal than minStudentCount({_minStudentCount})");
            //_maxStudentCount = _minStudentCount;

            //максимальное количество студентов не может быть установлено меньше, чем уже записано
            if (maxStudentCount > 0 && maxStudentCount < StudentsCount)
                //_maxStudentCount = StudentsCount;
                throw new LessonParameterException("Validation failed for lesson init", nameof(maxStudentCount),
                    $"maxStudentCount({maxStudentCount}) must be greater or equal than StudentsCount ({StudentsCount})");

            _maxStudentCount = maxStudentCount;
            return CheckStatus();
        }

        #endregion


        #region Действия с уроком для учеников

        public Student SubscribeStudent(Guid studentId)
        {
            switch (Status)
            {
                case LessonStatus.Expired:
                    throw new LessonException(
                        $"Срок проведения занятия истек {_startDate.ToString(CultureInfo.CurrentCulture)}");
                case LessonStatus.Pending:
                    throw new LessonException(
                        "Количество записанных учеников достигло максимума. Урок закрыт для записи новых учеников");
                case LessonStatus.Canceled:
                case LessonStatus.Completed:
                    throw new LessonException("Урок закончился и закрыт для записи новых учеников");
            }

            var student = Students.FirstOrDefault(c => c.PublicId == studentId);
            if (student != null) throw new LessonException($"Student with id='{studentId}' is subscribed already");

            //if (Students.All(c => c.PublicId != studentId))
            student = new Student()
            {
                Lesson = this,
                LessonId = Id,
                PublicId = studentId
            };
            Students.Add(student);
            CheckStatus();
            return student;
        }

        public Student UnsubscribeStudent(Guid studentId)
        {
            switch (Status)
            {
                case LessonStatus.Expired:
                    throw new LessonException(
                        $"Срок проведения занятия истек {_startDate.ToString(CultureInfo.CurrentCulture)}");
                case LessonStatus.Canceled:
                case LessonStatus.Completed:
                    throw new LessonException("Урок закончился и закрыт для записи новых учеников");
            }

            if (Students.Count == 0)
                throw new LessonException(
                    $"Количество записанных учеников урока с id='{Id}'  равно 0. Невозможно отписать ученика.");

            var student = GetSubscribedStudent(studentId);
            if (student.IsAttendaned) throw new LessonException("Невозможно отписаться от урока после его запуска");

            Students.Remove(student);
            if (ByRequest && StudentsCount == 0) CancelLessonBase(studentId, "Removed all students");

            CheckStatus();
            return student;
        }

        /// <summary>
        /// Присоединение ученика к уроку для фиксации его участия в нем
        /// </summary>
        public string JoinLesson(Guid studentId)
        {
            switch (Status)
            {
                case LessonStatus.Expired:
                    throw new LessonException(
                        $"Срок проведения занятия истек {_startDate.ToString(CultureInfo.CurrentCulture)}");
                case LessonStatus.Started:
                    var student = GetSubscribedStudent(studentId);
                    student.IsAttendaned = true;
                    return _inviteLink;
                default:
                    throw new LessonException(
                        $"Урок не запущен и имеет статус '{_status.ToString()}'.  Невозможно присоединиться к уроку.");
            }
        }

        /// <summary>
        /// Получение ссылки на трансляцию учеником
        /// </summary>
        public string GetInviteLink(Guid userId)
        {
            if (userId == TeacherId && _inviteLink != null) return _inviteLink;
            if (_status != LessonStatus.Started)
                throw new LessonException(
                    $"Урок не запущен и имеет статус '{Status.ToString()}'.  Невозможно получить ссылку к уроку.");
            var student = GetSubscribedStudent(userId);

            if (!student.IsAttendaned)
                throw new LessonException(
                    $"Студент с id='{userId}' не присоединился к уроку с id='{Id}', поэтому не может получить доступ к ссылке на него");

            return _inviteLink;
        }

        /// <summary>
        /// получить по id объект студента, записанного на урок 
        /// </summary>
        /// <exception cref="StudentIsNotSubscribedException">студент не был в списке
        /// записавшихся на этот урок, поэтому не может получить к нему доступ</exception>
        public Student GetSubscribedStudent(Guid studentId)
        {
            var student = Students.FirstOrDefault(c => c.PublicId == studentId);
            if (student == null)
                throw new LessonException(
                    $"Студент с id='{studentId}' не записан на урок с id='{Id}', поэтому не может получить к нему доступ");

            return student;
        }

        #endregion
    }
}