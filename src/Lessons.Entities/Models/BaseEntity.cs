using System;
using System.ComponentModel.DataAnnotations;

namespace Lessons.Entities.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }

        public BaseEntity() : this(Guid.NewGuid())
        {
        }

        public BaseEntity(Guid guid)
        {
            Id = guid;
            Created = DateTime.Now;
        }
    }
}