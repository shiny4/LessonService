using System;
using System.Collections.Generic;
using System.Linq;
using Lessons.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace Lessons.Entities.Models
{
    public abstract class LessonBase : BaseEntity
    {
        #region Constructors

        protected LessonBase(string lessonTitle, Guid teacherId, DateTime startDate)
            : this(Guid.NewGuid(), lessonTitle, teacherId, startDate)
        {
        }

        protected LessonBase(Guid guid, string lessonTitle, Guid teacherId, DateTime startDate) : base(guid)
        {
            TeacherId = teacherId;
            Students = new List<Student>();
            _status = LessonStatus.OpenedForSubscribe;
            _lessonTitle = lessonTitle;
            _startDate = startDate;
            _parentRequestId = default;
        }

        #endregion

        #region Fields and Propeties

        protected string _inviteLink;
        protected DateTime _startDate;
        protected int _maxStudentCount = 0;
        protected int _minStudentCount = 1;
        protected int _durationInMin = 60;
        protected string _lessonTitle;
        protected string _lessonDescription = "";
        protected decimal _price = 100m;
        protected LessonStatus _status;
        internal Guid _parentRequestId;

        [Display(Name = "Состояние урока")]
        public LessonStatus Status
        {
            get => _status;
            protected set => _status = value;
        }

        [Display(Name = "Название урока")]
        public string LessonTitle
        {
            get => _lessonTitle;
            protected set => _lessonTitle = value;
        }

        [Display(Name = "Описание урока")]
        public string LessonDescription
        {
            get => _lessonDescription;
            protected set => _lessonDescription = value;
        }

        [Display(Name = "Время начала урока")]
        public DateTime StartDate
        {
            get => _startDate;
            protected set => _startDate = value;
        }

        [Display(Name = "Длительность урока в минутах")]
        public int DurationInMin
        {
            get => _durationInMin;
            protected set => _durationInMin = value;
        }

        public decimal Price
        {
            get => _price;
            protected set => _price = value;
        }

        [Display(Name = "Максимальное количество учеников")]
        public int MaxStudentCount
        {
            get => _maxStudentCount;
            protected set => _maxStudentCount = value;
        }

        [Display(Name = "Минимальное количество учеников")]
        public int MinStudentCount
        {
            get => _minStudentCount;
            protected set => _minStudentCount = value;
        }

        [Display(Name = "Заявка на урок")]
        public Guid ParentRequestId
        {
            get => _parentRequestId;
            init => _parentRequestId = value;
        } //родительская заявка

        [Display(Name = "Ид преподавателя")] public Guid TeacherId { get; init; }

        #endregion

        #region Relations

        [Display(Name = "Список учеников")] public virtual ICollection<Student> Students { get; protected set; }

        [Display(Name = "Информация об отмене урока")]
        public virtual CanceledLesson СanceledLesson { get; protected set; }

        public Guid? CanceledLessonId { get; protected set; }

        #endregion

        #region Calcutated Fields

        [Display(Name = "время окончания урока")]
        public DateTime? EndDate => _startDate.AddMinutes(_durationInMin);

        [Display(Name = "Количество учеников, записанных на урок")]
        public int StudentsCount => Students.Count;

        [Display(Name = "Список ид учеников, записанных на урок")]
        public List<Guid> SubscribedStudentIds => Students.Select(c => c.PublicId).ToList();

        [Display(Name = "Список ид учеников, присоединившихся к уроку")]
        public List<Guid> AttendanedStudentIds =>
            Students.Where(c => c.IsAttendaned == true).Select(c => c.PublicId).ToList();

        [Display(Name = "урок создан по предварительной заявке")]
        public bool ByRequest => ParentRequestId != default;

        #endregion
    }
}