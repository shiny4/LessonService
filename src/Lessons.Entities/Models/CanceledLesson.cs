using System;
using Lessons.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace Lessons.Entities.Models
{
    public class CanceledLesson : BaseEntity
    {
        [Display(Name = "Ид урока")] public Guid LessonId { get; set; }
        public virtual LessonWithState Lesson { get; set; }
        public Guid InitiatorId { get; set; }

        [Display(Name = "Дата отмены урока")] public DateTime CancelDate { get; set; }

        [Display(Name = "Причина отмены урока")]
        public string Reason { get; set; }
    }
}