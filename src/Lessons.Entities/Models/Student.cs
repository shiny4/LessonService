using System;
using System.ComponentModel.DataAnnotations;

namespace Lessons.Entities.Models
{
    public class Student : BaseEntity
    {
        public Guid PublicId { get; set; }

        [Display(Name = "Присоединился к уроку")]
        public bool IsAttendaned { get; set; } = false; //был на занятии

        [Display(Name = "Ид урока")] public Guid LessonId { get; set; }
        public virtual LessonWithState Lesson { get; set; }
        public bool IsPaid { get; set; } = false; // оплатил занятие
    }
}