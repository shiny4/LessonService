using System;

namespace Lessons.Entities.Models
{
    public class StatusControlItem : BaseEntity
    {
        public Guid LessonWithStateId { get; set; }
        public virtual LessonWithState LessonWithState { get; set; }
        public DateTime StartDateTime { get; set; }
        public bool IsProcessed { get; set; } = false;
        public bool IsRejected { get; set; } = false;
        public DateTime? LastProcessTime { get; set; } = null;
        public int Attempts { get; set; } = 0;
    }
}