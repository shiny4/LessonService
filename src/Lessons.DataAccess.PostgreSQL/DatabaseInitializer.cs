using System.Linq;
using Lessons.DataAccess.Data;

namespace Lessons.DataAccess
{
    public class DatabaseInitializer
    {
        private readonly DataContext _dataContext;

        public DatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public DatabaseInitializer()
        {
            _dataContext = null;
        }

        public void Refresh()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureCreated();

            var lesson = _dataContext.Lessons.FirstOrDefault(b => b.LessonTitle == "New topic 1");
            if (lesson == null) AddContent(_dataContext);
        }

        public void Initialize(DataContext dbContext)
        {
            dbContext.Database.EnsureCreated();

            var lesson = dbContext.Lessons.FirstOrDefault(b => b.LessonTitle == "New topic 1");
            if (lesson == null) AddContent(dbContext);
        }

        public void AddContent(DataContext dbContext)
        {
            dbContext.Lessons.AddRange(FakeDataFactory.Lessons);
            dbContext.Students.AddRange(FakeDataFactory.Students);
            dbContext.CanceledLessons.AddRange(FakeDataFactory.CanceledLessons);

            // //dbContext.Database.Migrate();
            dbContext.SaveChanges();
        }
    }
}