﻿using Lessons.DataAccess.EntityConfigurations;
using Lessons.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Lessons.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<LessonWithState> Lessons { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<CanceledLesson> CanceledLessons { get; set; }

        public DbSet<Email> Emails { get; set; }
        public DbSet<StatusControlItem> StatusControlItems { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LessonEntityConfiguration());
            modelBuilder.ApplyConfiguration(new StudentEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CanceledLessonEntityConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}