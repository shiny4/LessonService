﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Lessons.Entities.Models;

namespace Lessons.DataAccess.EntityConfigurations
{
    public class LessonEntityConfiguration : IEntityTypeConfiguration<LessonWithState>
    {
        public void Configure(EntityTypeBuilder<LessonWithState> builder)
        {
            builder.ToTable("Lessons");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.StartDate).IsRequired();
            builder.Property(r => r.LessonDescription).IsRequired(false).HasMaxLength(400);
            builder.Property(r => r.Status).IsRequired();
            builder.Property(r => r.DurationInMin).IsRequired();
            builder.Property(r => r.TeacherId).IsRequired();
            builder.Property("_inviteLink").HasColumnName("InviteLink");
            builder.Property(r => r.MaxStudentCount).IsRequired();
            builder.Property(r => r.MinStudentCount).IsRequired();
            builder.Property(r => r.LessonTitle).IsRequired();
            builder.Property(r => r.Price).IsRequired();
            builder.Property(r => r.ParentRequestId);
            builder.Property(r => r.Created);


            builder
                .HasMany(s => s.Students)
                .WithOne(g => g.Lesson)
                .HasForeignKey(s => s.LessonId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(s => s.СanceledLesson)
                .WithOne(g => g.Lesson)
                .HasForeignKey<CanceledLesson>(s => s.LessonId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}