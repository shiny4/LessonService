using Lessons.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lessons.DataAccess.EntityConfigurations
{
    public class CanceledLessonEntityConfiguration : IEntityTypeConfiguration<CanceledLesson>
    {
        public void Configure(EntityTypeBuilder<CanceledLesson> builder)
        {
            builder.ToTable("CanceledLessons");

            builder.Property(r => r.CancelDate).IsRequired();
            builder.Property(r => r.LessonId).IsRequired();
            builder.Property(r => r.InitiatorId).IsRequired();
            builder.Property(r => r.Reason);
            builder.Property(r => r.Created);
        }
    }
}