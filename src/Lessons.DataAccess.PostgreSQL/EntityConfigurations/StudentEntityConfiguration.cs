using Lessons.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Lessons.DataAccess.EntityConfigurations
{
    public class StudentEntityConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("Students");

            builder.Property(r => r.PublicId).IsRequired();
            builder.Property(r => r.LessonId).IsRequired();
            builder.Property(r => r.IsAttendaned).IsRequired();
            builder.Property(r => r.IsPaid).IsRequired();
            builder.Property(r => r.Created);
        }
    }
}