using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lessons.DataAccess
{
    public static class ServiceExtensions
    {
        public static void AddDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<DataContext>(options =>
                    options.UseInMemoryDatabase("LessonDb"));
            }
            else
            {
                services.AddDbContext<DataContext>(options =>
                {
                    options
                        .UseNpgsql(configuration.GetConnectionString("PostgreConnection"),
                            x => { x.MigrationsAssembly("Lessons.Web"); })
                        .UseLazyLoadingProxies();
                });

                services.AddScoped<DatabaseInitializer>();
            }
        }
    }
}