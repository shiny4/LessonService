using System;
using System.Collections.Generic;
using Lessons.Entities.Models;

namespace Lessons.DataAccess.Data
{
    public class FakeDataFactory
    {
        private static string _teacherId1 = "2c73101f-d2d4-46e4-9e29-93459dc71bc7";
        private static string _teacherId2 = "0d3acdc7-ee94-4e22-8edc-aaaf92072eac";

        private static string _student1 = "4122d412-682a-4bad-9837-ceda4824e261";
        private static string _student2 = "c2589eaf-3bc2-43d1-a101-bad3248fa4e3";
        private static string _student3 = "21092523-4d24-4909-b774-751de633ed95";
        private static string _student4 = "2122da12-672a-4ba1-9897-ceda0824e261";
        private static string _student5 = "2c73101f-d2d4-46e4-9e29-93459dc71bc7";

        private static string _lessonId1 = "5a3a7b57-3f09-4763-a375-b9f7c1699fd9";
        private static string _lessonId2 = "5b94ba37-52c3-4ffd-9968-795c60e9b0ba";
        private static string _lessonId3 = "437ac77f-d0c1-4120-b391-18fb9ec5301e";
        private static string _lessonId4 = "b25a9294-35eb-447d-9717-6abcf3e4ad64";
        private static string _lessonId5 = "b25a9295-59eb-447d-9112-6abca3e4ad64";
        private static string _lessonId6 = "a8a17dac-85bf-44ae-be0e-a8f41c1d5ab6";
        private static string _lessonId7 = "84ecf821-239a-4fdb-8005-6d305db4a3de";

        private static string _parentRequestId1 = "1c7dab91-3317-41a5-adf5-8682fa3d12af";
        private static string _parentRequestId2 = "b296fb4f-e1fa-401b-907f-f28a88f47cf6";
        private static string _parentRequestId3 = "2ec1dcdc-b2b5-4a9e-8ace-33a8ae240f0f";

        private static string _CanceledLesson1 = "dfeba61e-8c05-4665-bdb2-5a6c4418b528";

        public static List<LessonWithState> Lessons
        {
            get
            {
                var result = new List<LessonWithState>()
                {
                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId1),
                        teacherId: Guid.Parse(_teacherId1),
                        startDate: DateTime.Now.AddDays(5),
                        lessonTitle: "New topic 1",
                        lessonDescription: "New topic 1 Description",
                        durationInMin: 90,
                        maxStudentCount: 5,
                        minStudentCount: 2,
                        price: 100),
                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId2),
                        teacherId: Guid.Parse(_teacherId1),
                        startDate: DateTime.Now.AddDays(1),
                        lessonTitle: "New topic 2",
                        lessonDescription: "New topic 2 Description",
                        durationInMin: 60,
                        maxStudentCount: 4,
                        minStudentCount: 1,
                        price: 200),

                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId3),
                        teacherId: Guid.Parse(_teacherId1),
                        startDate: DateTime.Now.AddDays(2),
                        lessonTitle: "New topic 4",
                        lessonDescription: "New topic 4 Description",
                        durationInMin: 120,
                        maxStudentCount: 0,
                        minStudentCount: 0,
                        price: 1400),

                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId4),
                        teacherId: Guid.Parse(_teacherId1),
                        startDate: DateTime.Now.AddDays(8),
                        lessonTitle: "New topic 3",
                        lessonDescription: "New topic 3 Description",
                        durationInMin: 80,
                        maxStudentCount: 0,
                        minStudentCount: 0,
                        price: 1000),

                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId5),
                        parentRequestId: Guid.Parse(_parentRequestId1),
                        teacherId: Guid.Parse(_teacherId1),
                        startDate: DateTime.Now.AddDays(8),
                        lessonTitle: "New topic 8",
                        lessonDescription: "New topic 8 Description",
                        durationInMin: 80,
                        price: 1000),

                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId6),
                        parentRequestId: Guid.Parse(_parentRequestId2),
                        teacherId: Guid.Parse(_teacherId2),
                        startDate: DateTime.Now.AddDays(4),
                        lessonTitle: "New topic 5",
                        lessonDescription: "New topic 5 Description",
                        durationInMin: 45,
                        price: 500),

                    LessonWithState.CreateUnboundLesson(
                        id: Guid.Parse(_lessonId7),
                        parentRequestId: Guid.Parse(_parentRequestId3),
                        teacherId: Guid.Parse(_teacherId2),
                        startDate: DateTime.Now.AddDays(7),
                        lessonTitle: "New topic 6",
                        lessonDescription: "New topic 6 Description",
                        durationInMin: 50,
                        price: 1200)
                };

                result[6].CancelLesson(result[6].TeacherId, "bad season");
                return result;
            }
        }

        public static List<CanceledLesson> CanceledLessons => new()
        {
            new CanceledLesson()
            {
                Id = Guid.Parse(_CanceledLesson1),
                InitiatorId = Guid.Parse(_teacherId1),
                LessonId = Guid.Parse(_lessonId7),
                Reason = "sfsdfsd sdfsdfsdf",
                CancelDate = DateTime.Now
            }
        };

        public static List<Student> Students => new()
        {
            new Student()
            {
                LessonId = Guid.Parse(_lessonId1),
                PublicId = Guid.Parse(_student1)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId1),
                PublicId = Guid.Parse(_student4)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId1),
                PublicId = Guid.Parse(_student3)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId1),
                PublicId = Guid.Parse(_student4)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId2),
                PublicId = Guid.Parse(_student3)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId2),
                PublicId = Guid.Parse(_student1)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId2),
                PublicId = Guid.Parse(_student2)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId4),
                PublicId = Guid.Parse(_student2)
            },
            new Student()
            {
                LessonId = Guid.Parse(_lessonId6),
                PublicId = Guid.Parse(_student5)
            }
        };
    }
}