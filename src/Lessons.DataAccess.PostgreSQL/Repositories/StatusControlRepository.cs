using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Lessons.DataAccess.Repositories
{
    public class StatusControlRepository : BaseRepository<StatusControlItem>, IStatusControlRepository
    {
        public StatusControlRepository(DataContext context) : base(context)
        {
        }

        public async Task<ICollection<StatusControlItem>> GetActiveJobsAsync()
        {
            var dateNow = DateTime.Now;
            var list = DbSet.Where(x =>
                !x.IsProcessed &&
                x.Attempts < 3 &&
                !x.IsRejected &&
                x.StartDateTime < dateNow).Include(x => x.LessonWithState);
            return await list.ToListAsync();
        }
    }
}