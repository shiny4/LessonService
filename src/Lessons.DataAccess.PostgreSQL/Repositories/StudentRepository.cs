using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Lessons.DataAccess.Repositories
{
    public class StudentRepository : BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(DataContext context) : base(context)
        {
        }
    }
}