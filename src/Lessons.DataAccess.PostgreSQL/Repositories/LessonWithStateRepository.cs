﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Lessons.DataAccess.Repositories
{
    public class LessonWithStateRepository : BaseRepository<LessonWithState>, ILessonRepository<LessonWithState>
    {
        public LessonWithStateRepository(DataContext context) : base(context)
        {
        }

        public async Task<ICollection<LessonWithState>> GetByStudentIdAsync(Guid studentid)
        {
            return await DbSet.AsNoTracking().Where(e => e.Students.Any(c => c.PublicId == studentid)).ToListAsync();
        }

        public async Task<ICollection<LessonWithState>> GetByTeacherIdAsync(Guid teacherid)
        {
            return await DbSet.AsNoTracking().Where(e => e.TeacherId == teacherid).ToListAsync();
        }

        public async Task<ICollection<Guid>> GetStudentsByLessonIdAsync(Guid lessonid)
        {
            var lesson = await DbSet.SingleOrDefaultAsync(e => e.Id == lessonid);
            if (lesson != null) return lesson.Students.Select(e => e.PublicId).ToList();
            return new List<Guid>();
        }

        public async Task<ICollection<Guid>> GetAttendanedStudentsByLessonIdAsync(Guid lessonid)
        {
            var lesson = await DbSet.SingleOrDefaultAsync(e => e.Id == lessonid);
            if (lesson != null)
                return lesson.Students.Where(e => e.IsAttendaned == true).Select(e => e.PublicId).ToList();
            return new List<Guid>();
        }
    }
}