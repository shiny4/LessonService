using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Lessons.DataAccess.Repositories
{
    public class CanceledLessonRepository : BaseRepository<CanceledLesson>, ICanceledLessonRepository
    {
        public CanceledLessonRepository(DataContext context) : base(context)
        {
        }

        public async Task<CanceledLesson> GetInfoByLessonIdAsync(Guid lessonid)
        {
            return await DbSet.SingleOrDefaultAsync(e =>
                e.LessonId == lessonid);
        }
    }
}