using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Enums;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Students.Commands
{
    public static class UnSubscribeStudent
    {
        public record UnSubscribeStudentByLessonIdCommand(Guid LessonId, Guid StudentId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<UnSubscribeStudentByLessonIdCommand>
        {
            public Validator()
            {
                RuleFor(p => p.StudentId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<UnSubscribeStudentByLessonIdCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IStudentRepository _studentRepository;
            private readonly ICanceledLessonRepository _canceledLessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;
            private readonly IEmailService _emailService;

            public Handler(IMapper mapper,
                ILessonRepository<LessonWithState> lessonRepository,
                ILogger<Handler> logger,
                IEmailService emailService,
                IStudentRepository studentRepository,
                ICanceledLessonRepository canceledLessonRepository)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _logger = logger;
                _emailService = emailService;
                _studentRepository = studentRepository;
                _canceledLessonRepository = canceledLessonRepository;
            }

            public async Task<OkResponse> Handle(UnSubscribeStudentByLessonIdCommand command,
                CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {Id} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                var student = lesson.UnsubscribeStudent(command.StudentId);
                await _studentRepository.DeleteAsync(student);
                if (lesson.Status == LessonStatus.Canceled)
                    await _canceledLessonRepository.UpdateAsync(lesson.СanceledLesson);

                await _lessonRepository.UpdateAsync(lesson);

                await _emailService.SendEmailAsync(lesson.Id, DateTime.Now, "Lesson subscription",
                    $"You have unsubscripted from lesson with id={lesson.Id} with title {lesson.LessonTitle} at {lesson.StartDate} ",
                    0);

                _logger.LogInformation("Student with id {StudentId} is unsubscribed from lesson with id={LessonId}",
                    command.StudentId, command.LessonId);
                return new OkResponse() {Status = 200, Count = 1, Content = "Unsubscribed"};
            }
        }
    }
}