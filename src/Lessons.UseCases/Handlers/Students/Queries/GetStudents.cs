using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Students.Queries
{
    public static class GetStudents
    {
        public record GetStudentsByLessonIdQuery(Guid LessonId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetStudentsByLessonIdQuery>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetStudentsByLessonIdQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetStudentsByLessonIdQuery query,
                CancellationToken cancellationToken)
            {
                var students = await _lessonRepository.GetStudentsByLessonIdAsync(query.LessonId);
                _logger.LogInformation("Found {Count} students for lesson with id={LessonId}",
                    students.Count, query.LessonId);
                return new OkResponse()
                    {Status = 200, Count = students.Count, Content = students};
            }
        }
    }
}