using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;


namespace Lessons.UseCases.Handlers.Students.Queries
{
    public static class GetInvitelink
    {
        public record GetInvitelinkByLessonIdQuery(Guid LessonId, Guid StudentId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetInvitelinkByLessonIdQuery>
        {
            public Validator()
            {
                RuleFor(p => p.StudentId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetInvitelinkByLessonIdQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetInvitelinkByLessonIdQuery query,
                CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(query.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {Id} wasn't found", query.LessonId);
                    throw new AppException($"Lesson with {query.LessonId} wasn't found", 404);
                }

                return new OkResponse()
                    {Status = 200, Count = 1, Content = lesson.GetInviteLink(query.StudentId)};
            }
        }
    }
}