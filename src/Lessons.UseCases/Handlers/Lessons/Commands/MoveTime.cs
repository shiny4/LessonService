using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class MoveTime
    {
        public record MoveLessonTimeCommand
            (Guid LessonId, Guid InitiatorId, DateTime StartDate, int DurationInMin) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<MoveLessonTimeCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.StartDate)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");

                RuleFor(p => p.DurationInMin)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(15).WithMessage("{PropertyName} must be greater 15 minut");
            }
        }

        public class Handler : IRequestHandler<MoveLessonTimeCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;
            private readonly ILogger<Handler> _logger;

            public Handler(IMapper mapper, ILessonRepository<LessonWithState> lessonRepository,
                IEmailService emailService, IStatusControl statusControl, ILogger<Handler> logger)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _emailService = emailService;
                _statusControl = statusControl;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(MoveLessonTimeCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                var isToChangeJob = command.StartDate != lesson.StartDate;

                lesson.MoveLessonTime(command.InitiatorId, command.StartDate, command.DurationInMin);
                await _lessonRepository.UpdateAsync(lesson);
                if (isToChangeJob)
                {
                    await _statusControl.ChangeLessonData(lesson.Id, lesson.StartDate);
                    await _emailService.ChangeEmailDate(lesson.Id, lesson.StartDate, 1);
                }

                _logger.LogInformation(
                    "Lesson with id {Id} was moved. Lesson start is {LessonStart}, lesson duration is {LessonDuration}",
                    lesson.Id, lesson.StartDate, lesson.DurationInMin);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}