using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Exceptions;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class Update
    {
        public record UpdateLessonCommand(Guid LessonId, Guid InitiatorId, string LessonTitle, string LessonDescription,
            DateTime StartDate, int DurationInMin, int MaxStudentCount, int MinStudentCount,
            decimal Price) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<UpdateLessonCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.LessonTitle)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .NotNull().WithMessage("{PropertyName} is required.")
                    .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                    .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");

                RuleFor(p => p.Price)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(100m).WithMessage("{PropertyName} must be greater 100");

                RuleFor(p => p.StartDate)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");

                RuleFor(p => p.DurationInMin)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(15).WithMessage("{PropertyName} must be greater 15 minut");

                RuleFor(p => p.MaxStudentCount)
                    .GreaterThanOrEqualTo(0).WithMessage("{PropertyName} must be greater or equal 0");

                RuleFor(p => p.MinStudentCount)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThanOrEqualTo(1).WithMessage("{PropertyName} must be greater 0");
            }
        }


        public class Handler : IRequestHandler<UpdateLessonCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;

            public Handler(IMapper mapper,
                ILessonRepository<LessonWithState> lessonRepository,
                ILogger<Handler> logger,
                IEmailService emailService,
                IStatusControl statusControl)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _logger = logger;
                _emailService = emailService;
                _statusControl = statusControl;
            }

            public async Task<OkResponse> Handle(UpdateLessonCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                var isToChangeJob = command.StartDate != lesson.StartDate;
                lesson.EditLesson(
                    command.InitiatorId,
                    command.LessonTitle,
                    command.LessonDescription,
                    command.StartDate,
                    command.DurationInMin,
                    command.MaxStudentCount,
                    command.MinStudentCount,
                    command.Price
                );
                await _lessonRepository.UpdateAsync(lesson);
                if (isToChangeJob)
                {
                    await _statusControl.ChangeLessonData(lesson.Id, lesson.StartDate);
                    await _emailService.ChangeEmailDate(lesson.Id, lesson.StartDate, 1);
                }

                _logger.LogInformation("Lesson with id {Id} was updated as {@Updatedlesson}", lesson.Id, lesson);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}