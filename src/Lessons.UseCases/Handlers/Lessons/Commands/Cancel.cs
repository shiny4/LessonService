using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class Cancel
    {
        public record CancelLessonCommand(Guid LessonId, Guid InitiatorId, string Reason) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<CancelLessonCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<CancelLessonCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly ICanceledLessonRepository _canceledLessonRepository;
            private readonly IMapper _mapper;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;
            private readonly ILogger<Handler> _logger;

            public Handler(IMapper mapper, ILessonRepository<LessonWithState> lessonRepository,
                IEmailService emailService, IStatusControl statusControl,
                ICanceledLessonRepository canceledLessonRepository, ILogger<Handler> logger)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _emailService = emailService;
                _statusControl = statusControl;
                _canceledLessonRepository = canceledLessonRepository;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(CancelLessonCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                lesson.CancelLesson(command.InitiatorId, command.Reason);
                await _canceledLessonRepository.CreateAsync(lesson.СanceledLesson);
                await _lessonRepository.UpdateAsync(lesson);

                await _statusControl.RejectJob(lesson.Id);
                await _emailService.RejectJob(lesson.Id, 0);
                await _emailService.ChangeEmailContent(lesson.Id, 1, DateTime.Now, "Cancelled lesson",
                    $"Lesson with id {lesson.Id} with title '{lesson.LessonTitle}' was canceled. Sorry");

                _logger.LogInformation("Lesson with id {Id} was canceled. Lesson status is {LessonStatus}", lesson.Id,
                    lesson.Status);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}