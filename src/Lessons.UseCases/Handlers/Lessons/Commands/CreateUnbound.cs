using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Exceptions;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class CreateUnbound
    {
        public record CreateUnboundLessonCommand(Guid TeacherId, string LessonTitle, string LessonDescription,
            DateTime StartDate, int DurationInMin, int MaxStudentCount, int MinStudentCount,
            decimal Price) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<CreateUnboundLessonCommand>
        {
            public Validator()
            {
                RuleFor(p => p.TeacherId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
                RuleFor(p => p.LessonTitle)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .NotNull().WithMessage("{PropertyName} is required.")
                    .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                    .MinimumLength(3).WithMessage("{PropertyName} must exceed 2 characters.");

                RuleFor(p => p.Price)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(100m).WithMessage("{PropertyName} must be greater 100");

                RuleFor(p => p.StartDate)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(DateTime.Now).WithMessage($"{{PropertyName}} must be greater {DateTime.Now}");

                RuleFor(p => p.DurationInMin)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(15).WithMessage("{PropertyName} must be greater 15 minut");

                RuleFor(p => p.MaxStudentCount)
                    .GreaterThanOrEqualTo(0).WithMessage("{PropertyName} must be greater or equal 0");

                RuleFor(p => p.MinStudentCount)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(0).WithMessage("{PropertyName} must be greater 0");
            }
        }

        public class Handler : IRequestHandler<CreateUnboundLessonCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly ILogger<Handler> _logger;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;

            public Handler(
                ILessonRepository<LessonWithState> lessonRepository, ILogger<Handler> logger,
                IEmailService emailService, IStatusControl statusControl)
            {
                _lessonRepository = lessonRepository;
                _logger = logger;
                _emailService = emailService;
                _statusControl = statusControl;
            }

            public async Task<OkResponse> Handle(CreateUnboundLessonCommand command,
                CancellationToken cancellationToken)
            {
                var lesson = LessonWithState.CreateUnboundLesson(
                    command.TeacherId,
                    command.LessonTitle,
                    command.StartDate,
                    command.DurationInMin,
                    command.LessonDescription,
                    command.MaxStudentCount,
                    command.MinStudentCount,
                    command.Price
                );
                var newId = await _lessonRepository.CreateAsync(lesson);
                await _statusControl.AddLessonToControl(newId, lesson.StartDate);

                await _emailService.SendEmailAsync(newId, DateTime.Now, "Lesson added",
                    $"Lesson added with id={lesson.Id} with title {lesson.LessonTitle} at {lesson.StartDate} ", 0);

                await _emailService.SendEmailAsync(newId, lesson.StartDate.AddMinutes(-30),
                    "Lesson will be started soon",
                    $"Lesson with id={lesson.Id} with title {lesson.LessonTitle} will be started {lesson.StartDate} ",
                    1);

                _logger.LogInformation("Lesson with id {Id} was created as {@Newlesson}", lesson.Id, lesson);
                return new OkResponse() {Status = 201, Count = 1, Content = new List<Guid> {newId}};
            }
        }
    }
}