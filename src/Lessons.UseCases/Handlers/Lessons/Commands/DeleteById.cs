using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class DeleteById
    {
        public record DeleteLessonByIdCommand(Guid LessonId, Guid InitiatorId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<DeleteLessonByIdCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<DeleteLessonByIdCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;

            public Handler(IMapper mapper, ILessonRepository<LessonWithState> lessonRepository, ILogger<Handler> logger,
                IStatusControl statusControl, IEmailService emailService)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _logger = logger;
                _statusControl = statusControl;
                _emailService = emailService;
            }

            public async Task<OkResponse> Handle(DeleteLessonByIdCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                if (lesson.TeacherId != command.InitiatorId)
                {
                    _logger.LogWarning("Lesson '{LessonId}' may be deleted only by teacher with id='{TeacherId}",
                        command.LessonId, lesson.TeacherId);
                    throw new AppException(
                        $"Lesson '{command.LessonId}' may be deleted only by teacher with id='{lesson.TeacherId}'",
                        403);
                }

                await _statusControl.DeleteJob(lesson.Id);
                await _emailService.DeleteJob(lesson.Id);
                await _lessonRepository.DeleteAsync(lesson);
                _logger.LogInformation("Lesson with id {Id} was deleted", lesson.Id);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}