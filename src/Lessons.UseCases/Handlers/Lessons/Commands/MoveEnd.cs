using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class MoveEnd
    {
        public record MoveLessonEndCommand(Guid LessonId, Guid InitiatorId, int DurationInMin) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<MoveLessonEndCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.DurationInMin)
                    .NotEmpty().WithMessage("{PropertyName} is required.")
                    .GreaterThan(15).WithMessage("{PropertyName} must be greater 15 minut");
            }
        }

        public class Handler : IRequestHandler<MoveLessonEndCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(IMapper mapper, ILessonRepository<LessonWithState> lessonRepository, ILogger<Handler> logger)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(MoveLessonEndCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                lesson.MoveLessonEnd(command.InitiatorId, command.DurationInMin);
                await _lessonRepository.UpdateAsync(lesson);

                _logger.LogInformation("Lesson with id {Id} was changed. Lesson  duration is {LessonDuration}",
                    lesson.Id, lesson.DurationInMin);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}