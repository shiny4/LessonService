using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Commands
{
    public static class Start
    {
        public record StartLessonCommand(Guid LessonId, Guid InitiatorId, string InviteLink) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<StartLessonCommand>
        {
            public Validator()
            {
                RuleFor(p => p.LessonId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");

                RuleFor(p => p.InitiatorId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<StartLessonCommand, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly IEmailService _emailService;
            private readonly IStatusControl _statusControl;
            private readonly ILogger<Handler> _logger;

            public Handler(IMapper mapper, ILessonRepository<LessonWithState> lessonRepository,
                IEmailService emailService, IStatusControl statusControl, ILogger<Handler> logger)
            {
                _mapper = mapper;
                _lessonRepository = lessonRepository;
                _emailService = emailService;
                _statusControl = statusControl;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(StartLessonCommand command, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(command.LessonId);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {LessonId} wasn't found", command.LessonId);
                    throw new AppException($"Lesson with {command.LessonId} wasn't found", 404);
                }

                lesson.StartLesson(command.InitiatorId, command.InviteLink);

                await _lessonRepository.UpdateAsync(lesson);

                _logger.LogInformation("Lesson with id {Id} was started. Lesson status is {LessonStatus}", lesson.Id,
                    lesson.Status);
                return new OkResponse() {Status = 200, Count = 1, Content = new List<Guid> {lesson.Id}};
            }
        }
    }
}