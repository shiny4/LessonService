using System;

namespace Lessons.UseCases.Handlers.Lessons.Dto
{
    public class CanceledLessonResponseDto
    {
        public Guid LessonId { get; set; }
        public string LessonTitle { get; set; }
        public string LessonDescription { get; set; }
        public DateTime StartDate { get; set; }
        public decimal Price { get; set; }
        public Guid TeacherId { get; set; }
        public int MaxStudentCount { get; set; }
        public int MinStudentCount { get; set; }
        public Guid ParentRequestId { get; set; }
        public Guid InitiatorId { get; set; }
        public DateTime CancelDate { get; set; }
        public string Reason { get; set; }
    }
}