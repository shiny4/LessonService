using System;

namespace Lessons.UseCases.Handlers.Lessons.Dto
{
    public class LessonResponseDTO
    {
        public Guid Id { get; set; }
        public string LessonTitle { get; set; }
        public string LessonDescription { get; set; }
        public DateTime StartDate { get; set; }
        public int DurationInMin { get; set; }
        public string Status { get; set; }
        public decimal Price { get; set; }
        public Guid TeacherId { get; set; }
        public int MaxStudentCount { get; set; }
        public int MinStudentCount { get; set; }
        public Guid ParentRequestId { get; set; }
    }
}