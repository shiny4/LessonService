using System;

namespace Lessons.UseCases.Handlers.Lessons.Dto
{
    public class ShortLessonResponseDTO
    {
        public Guid Id { get; set; }
        public string LessonTitle { get; set; }
        public DateTime StartDate { get; set; }
        public string Status { get; set; }
        public decimal Price { get; set; }
        public Guid TeacherId { get; set; }
    }
}