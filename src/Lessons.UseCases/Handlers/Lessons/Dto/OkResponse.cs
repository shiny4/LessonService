namespace Lessons.UseCases.Handlers.Lessons.Dto
{
    public class OkResponse
    {
        public int Status { get; set; }
        public int Count { get; set; }
        public object Content { get; set; }
    }
}