using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Queries
{
    public static class GetByTeacherId
    {
        public record GetByTeacherIdQuery(Guid TeacherId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetByTeacherIdQuery>
        {
            public Validator()
            {
                RuleFor(p => p.TeacherId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetByTeacherIdQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetByTeacherIdQuery query,
                CancellationToken cancellationToken)
            {
                var lessons = await _lessonRepository.GetByTeacherIdAsync(query.TeacherId);
                if (lessons == null)
                {
                    _logger.LogInformation("Found 0 lessons for teacher with id={TeacherId}", query.TeacherId);
                    return new OkResponse()
                        {Status = 200, Count = 0, Content = new List<ShortLessonResponseDTO>()};
                }

                _logger.LogInformation("Found {Count} lessons for teacher with id={TeacherId}", lessons.Count,
                    query.TeacherId);

                return new OkResponse()
                {
                    Status = 200, Count = lessons.Count,
                    Content = _mapper.Map<ICollection<ShortLessonResponseDTO>>(lessons)
                };
            }
        }
    }
}