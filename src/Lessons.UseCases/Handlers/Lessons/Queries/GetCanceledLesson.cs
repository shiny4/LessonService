using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;


namespace Lessons.UseCases.Handlers.Lessons.Queries
{
    public static class GetCanceledLesson
    {
        public record GetCanceledLessonQuery(Guid Id) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetCanceledLessonQuery>
        {
            public Validator()
            {
                RuleFor(p => p.Id)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetCanceledLessonQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly ICanceledLessonRepository _canceledLessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository,
                IMapper mapper, ICanceledLessonRepository canceledLessonRepository, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _canceledLessonRepository = canceledLessonRepository;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetCanceledLessonQuery query,
                CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(query.Id);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {Id} wasn't found", query.Id);
                    throw new AppException($"Lesson with {query.Id} wasn't found", 404);
                }

                var canceledlesson = await _canceledLessonRepository.GetInfoByLessonIdAsync(query.Id);

                if (canceledlesson == null)
                {
                    _logger.LogWarning("Lesson with {Id} wasn't canceled", query.Id);
                    throw new AppException($"Lesson with {query.Id} wasn't canceled", 400);
                }

                var responseDto = new CanceledLessonResponseDto
                {
                    LessonId = query.Id,
                    LessonTitle = lesson.LessonTitle,
                    Price = lesson.Price,
                    LessonDescription = lesson.LessonDescription,
                    StartDate = lesson.StartDate,
                    TeacherId = lesson.TeacherId,
                    MaxStudentCount = lesson.MaxStudentCount,
                    MinStudentCount = lesson.MinStudentCount,
                    ParentRequestId = lesson.ParentRequestId,
                    CancelDate = canceledlesson.CancelDate,
                    Reason = canceledlesson.Reason,
                    InitiatorId = canceledlesson.InitiatorId
                };

                _logger.LogInformation("Found 1 canceled lesson for id={Id}", query.Id);
                return new OkResponse()
                    {Status = 200, Count = 1, Content = new List<CanceledLessonResponseDto>() {responseDto}};
            }
        }
    }
}