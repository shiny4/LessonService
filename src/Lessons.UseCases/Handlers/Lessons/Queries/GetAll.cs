using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;


namespace Lessons.UseCases.Handlers.Lessons.Queries
{
    public static class GetAll
    {
        public record GetAllQuery : IRequest<OkResponse>;

        public class Handler : IRequestHandler<GetAllQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetAllQuery query,
                CancellationToken cancellationToken)
            {
                var lessons = await _lessonRepository.GetAllAsync();
                if (lessons == null)
                {
                    _logger.LogInformation("Found total 0 lessons by request");
                    return new OkResponse()
                        {Status = 200, Count = 0, Content = new List<ShortLessonResponseDTO>()};
                }

                _logger.LogInformation("Found total {Count} lessons by request", lessons.Count);
                return new OkResponse()
                {
                    Status = 200, Count = lessons.Count,
                    Content = _mapper.Map<ICollection<ShortLessonResponseDTO>>(lessons)
                };
            }
        }
    }
}