using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Exceptions;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;


namespace Lessons.UseCases.Handlers.Lessons.Queries
{
    public static class GetById
    {
        public record GetByIdQuery(Guid Id) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetByIdQuery>
        {
            public Validator()
            {
                RuleFor(p => p.Id)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetByIdQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetByIdQuery query, CancellationToken cancellationToken)
            {
                var lesson = await _lessonRepository.GetByIdAsync(query.Id);

                if (lesson == null)
                {
                    _logger.LogWarning("Lesson with {Id} wasn't found", query.Id);
                    throw new AppException($"Lesson with {query.Id} wasn't found", 404);
                }

                _logger.LogInformation("Found 1 lesson for id={Id}", query.Id);
                return new OkResponse()
                {
                    Status = 200, Count = 1,
                    Content = new List<LessonResponseDTO>() {_mapper.Map<LessonResponseDTO>(lesson)}
                };
            }
        }
    }
}