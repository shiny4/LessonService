using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Lessons.UseCases.Handlers.Lessons.Dto;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Handlers.Lessons.Queries
{
    public static class GetByStudentId
    {
        public record GetByStudentIdQuery(Guid StudentId) : IRequest<OkResponse>;

        public class Validator : AbstractValidator<GetByStudentIdQuery>
        {
            public Validator()
            {
                RuleFor(p => p.StudentId)
                    .NotEmpty().WithMessage("{PropertyName} is required.");
            }
        }

        public class Handler : IRequestHandler<GetByStudentIdQuery, OkResponse>
        {
            private readonly ILessonRepository<LessonWithState> _lessonRepository;
            private readonly IMapper _mapper;
            private readonly ILogger<Handler> _logger;

            public Handler(ILessonRepository<LessonWithState> lessonRepository, IMapper mapper, ILogger<Handler> logger)
            {
                _lessonRepository = lessonRepository;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<OkResponse> Handle(GetByStudentIdQuery query,
                CancellationToken cancellationToken)
            {
                var lessons = await _lessonRepository.GetByStudentIdAsync(query.StudentId);
                if (lessons == null)
                {
                    _logger.LogInformation("Found 0 lessons for student with id={StudentId}", query.StudentId);
                    return new OkResponse()
                        {Status = 200, Count = 0, Content = new List<ShortLessonResponseDTO>()};
                }

                _logger.LogInformation("Found {Count} lessons for student with id={StudentId}", lessons.Count,
                    query.StudentId);
                return new OkResponse()
                {
                    Status = 200, Count = lessons.Count,
                    Content = _mapper.Map<ICollection<ShortLessonResponseDTO>>(lessons)
                };
            }
        }
    }
}