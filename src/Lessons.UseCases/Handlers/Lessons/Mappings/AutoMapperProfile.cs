using AutoMapper;
using Lessons.Entities.Models;
using Lessons.UseCases.Handlers.Lessons.Dto;

namespace Lessons.UseCases.Handlers.Lessons.Mappings
{
    public class LessonMapperProfile : Profile
    {
        public LessonMapperProfile()
        {
            CreateMap<LessonWithState, LessonResponseDTO>();
            CreateMap<LessonWithState, ShortLessonResponseDTO>();
        }
    }
}