using System.Reflection;
using FluentValidation;
using Lessons.UseCases.Behaviours;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Lessons.UseCases
{
    public static class ServiceExtensions
    {
        public static void AddUseCaseLayer(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        }
    }
}