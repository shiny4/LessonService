using System;
using System.Collections.Generic;
using FluentValidation.Results;
using Microsoft.Extensions.Logging;

//using Microsoft.Extensions.Logging;

namespace Lessons.UseCases.Exceptions
{
    public class LessonValidationException : Exception
    {
        private const string ValidationMessage =
            "One or more validation failures have occurred while creating or updating lesson.";

        public record ErrorValue(string PropertyName, string ErrorMessage);

        public List<ErrorValue> Errors { get; private set; }
        public string RequestName { get; private set; }
        public int? StatusCode { get; set; } = 400;

        public LessonValidationException(string message)
            : base(message)
        {
            Errors = new List<ErrorValue>();
        }

        public LessonValidationException() : this(ValidationMessage)
        {
        }

        public LessonValidationException(string message, int statusCode)
            : this(message)
        {
            StatusCode = statusCode;
        }

        public LessonValidationException(string message, int statusCode, string requestName,
            IEnumerable<ValidationFailure> failures) :
            this(message, statusCode)
        {
            RequestName = requestName;
            foreach (var failure in failures)
                Errors.Add(new ErrorValue(failure.PropertyName, failure.ErrorMessage));
        }

        public LessonValidationException(string message, string requestName, IEnumerable<ValidationFailure> failures)
            : this(message, 400, requestName, failures)
        {
        }

        public LessonValidationException(string requestName, IEnumerable<ValidationFailure> failures)
            : this(ValidationMessage, 400, requestName, failures)
        {
        }
    }
}