using System;

namespace Lessons.UseCases.Exceptions
{
    public class AppException : Exception
    {
        public int? StatusCode { get; set; } = 400;

        public AppException()
        {
        }

        public AppException(string message)
            : base(message)
        {
        }

        public AppException(string message, int statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public AppException(string message, Exception exception)
            : base(message, exception)
        {
        }

        public AppException(string message, int statusCode, Exception exception)
            : base(message, exception)
        {
            StatusCode = statusCode;
        }
    }
}