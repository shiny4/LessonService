using System;
using System.Threading.Tasks;
using Lessons.Entities.Models;

namespace Lessons.Infrastructure.Interfaces.DataAccess
{
    public interface ICanceledLessonRepository : IBaseRepository<CanceledLesson>
    {
        Task<CanceledLesson> GetInfoByLessonIdAsync(Guid lessonid);
    }
}