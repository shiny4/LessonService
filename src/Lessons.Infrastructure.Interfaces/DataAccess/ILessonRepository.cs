﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lessons.Entities.Models;

namespace Lessons.Infrastructure.Interfaces.DataAccess
{
    public interface ILessonRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : LessonBase
    {
        Task<ICollection<TEntity>> GetByStudentIdAsync(Guid studentid);
        Task<ICollection<TEntity>> GetByTeacherIdAsync(Guid teacherid);
        Task<ICollection<Guid>> GetStudentsByLessonIdAsync(Guid lessonid);

        Task<ICollection<Guid>> GetAttendanedStudentsByLessonIdAsync(Guid lessonid);
    }
}