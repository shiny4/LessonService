using Lessons.Entities.Models;

namespace Lessons.Infrastructure.Interfaces.DataAccess
{
    public interface IStudentRepository : IBaseRepository<Student>
    {
    }
}