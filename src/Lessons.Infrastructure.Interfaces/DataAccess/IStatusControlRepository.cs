using System.Collections.Generic;
using System.Threading.Tasks;
using Lessons.Entities.Models;

namespace Lessons.Infrastructure.Interfaces.DataAccess
{
    public interface IStatusControlRepository : IBaseRepository<StatusControlItem>
    {
        Task<ICollection<StatusControlItem>> GetActiveJobsAsync();
    }
}