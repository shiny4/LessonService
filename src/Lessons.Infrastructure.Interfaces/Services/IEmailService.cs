using System;
using System.Threading.Tasks;

namespace Lessons.Infrastructure.Interfaces.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(Guid lessonId, DateTime startDateTime, string subject, string body, int category = 0,
            string email = "");

        Task ChangeEmailDate(Guid lessonId, DateTime newStartDateTime, int category);
        Task ChangeEmailContent(Guid lessonId, int category, DateTime newStartDateTime, string subject, string body);
        Task RejectJob(Guid lessonId, int category);

        Task DeleteJob(Guid lessonId);
    }
}