using System;
using System.Threading.Tasks;

namespace Lessons.Infrastructure.Interfaces.Services
{
    public interface IStatusControl
    {
        Task AddLessonToControl(Guid lessonId, DateTime eventDate);
        Task ChangeLessonData(Guid lessonId, DateTime newStartDateTime);
        Task RejectJob(Guid lessonId);

        Task DeleteJob(Guid lessonId);
    }
}