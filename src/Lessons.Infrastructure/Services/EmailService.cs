using System;
using System.Linq;
using System.Threading.Tasks;
using Lessons.DataAccess;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.Services;
using Microsoft.Extensions.Logging;

namespace Lessons.Infrastructure.Services
{
    public class EmailService : IEmailService
    {
        private readonly DataContext _dbContext;
        private readonly ICurrentUserService _currentUserService;
        private readonly ILogger<EmailService> _logger;

        public EmailService(DataContext dbContext, ICurrentUserService currentUserService, ILogger<EmailService> logger)
        {
            _dbContext = dbContext;
            _currentUserService = currentUserService;
            _logger = logger;
        }

        public async Task SendEmailAsync(Guid lessonId, DateTime startDateTime, string subject, string body,
            int category = 0,
            string email = "")
        {
            if (string.IsNullOrWhiteSpace(email)) email = _currentUserService.Email;
            if (string.IsNullOrWhiteSpace(email)) return;
            if (lessonId == Guid.Empty) return;
            if (startDateTime <= DateTime.Now && category > 0) return;
            if (string.IsNullOrWhiteSpace(subject)) return;

            try
            {
                var newMail = new Email
                {
                    Address = email,
                    Subject = subject,
                    Body = $"Dear {_currentUserService.Name},  {body}. Thank you for choosing us.",
                    LessonWithStateIdId = lessonId,
                    StartDateTime = startDateTime,
                    Category = category
                };
                await _dbContext.Emails.AddAsync(newMail);
                await _dbContext.SaveChangesAsync();
                _logger.LogInformation("Mailing job for lesson id={Lessonid}  was added to date {NewStartDate}",
                    lessonId, startDateTime);
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    "Error in {ErrorName} occured during adding mailing job for lesson id={Lessonid}. Error: {ErrorDescription}",
                    ex.Source, lessonId, ex.Message);
            }
        }

        public async Task ChangeEmailDate(Guid lessonId, DateTime newStartDateTime, int category)
        {
            if (lessonId == Guid.Empty) return;
            if (newStartDateTime < DateTime.Now && category > 0) return;
            //var dateTimeNow = DateTime.Now;
            foreach (var item in _dbContext.Emails.Where(
                x => x.LessonWithStateIdId == lessonId && category == x.Category && !x.IsSended))
                try
                {
                    item.StartDateTime = newStartDateTime;
                    item.Attempts = 0;
                    item.IsRejected = false;
                    _logger.LogInformation(
                        "Email start date for lesson id={Lessonid}  was changed to value {NewStartDate}",
                        item.LessonWithStateIdId, item.StartDateTime);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during updating email job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, item.LessonWithStateIdId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }

        public async Task ChangeEmailContent(Guid lessonId, int category, DateTime newStartDateTime, string subject,
            string body)
        {
            if (lessonId == Guid.Empty) return;
            if (newStartDateTime < DateTime.Now && category > 0) return;
            foreach (var item in _dbContext.Emails.Where(
                x => x.LessonWithStateIdId == lessonId && category == x.Category && !x.IsSended))
                try
                {
                    item.Subject = subject;
                    item.Body = body;
                    item.StartDateTime = newStartDateTime;
                    item.Attempts = 0;
                    item.IsRejected = false;
                    _logger.LogInformation(
                        "Email content for lesson id={Lessonid}  was changed", item.LessonWithStateIdId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during updating email job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, item.LessonWithStateIdId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }

        public async Task RejectJob(Guid lessonId, int category)
        {
            if (lessonId == Guid.Empty) return;
            foreach (var item in _dbContext.Emails.Where(
                x => x.LessonWithStateIdId == lessonId && !x.IsRejected && !x.IsSended))
                try
                {
                    item.IsRejected = true;
                    _logger.LogInformation("Email job for lesson id={Lessonid}  was rejected at {Date}",
                        item.LessonWithStateIdId, DateTime.Now);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during rejecting email job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, item.LessonWithStateIdId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteJob(Guid lessonId)
        {
            if (lessonId == Guid.Empty) return;
            foreach (var item in _dbContext.Emails.Where(
                x => x.LessonWithStateIdId == lessonId))
                try
                {
                    _dbContext.Emails.Remove(item);
                    _logger.LogInformation("Email job for lesson id={Lessonid}  was deleted at {Date}",
                        item.LessonWithStateIdId, DateTime.Now);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during deleting email job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, item.LessonWithStateIdId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }
    }
}