﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Lessons.DataAccess;
using Lessons.Entities.Models;
using Lessons.Infrastructure.Interfaces.Services;
using Microsoft.Extensions.Logging;

namespace Lessons.Infrastructure.Services
{
    public class StatusControlService : IStatusControl
    {
        private readonly DataContext _dbContext;
        private readonly ILogger<StatusControlService> _logger;

        public StatusControlService(DataContext dbContext, ILogger<StatusControlService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddLessonToControl(Guid lessonId, DateTime startDateTime)
        {
            if (lessonId == Guid.Empty) return;
            if (startDateTime <= DateTime.Now) return;
            try
            {
                var item = new StatusControlItem()
                {
                    LessonWithStateId = lessonId,
                    StartDateTime = startDateTime,
                    Attempts = 0,
                    IsProcessed = false,
                    LastProcessTime = default
                };

                await _dbContext.StatusControlItems.AddAsync(item);
                await _dbContext.SaveChangesAsync();
                _logger.LogInformation("Job for lesson id={Lessonid}  was added to date {NewStartDate}",
                    lessonId, startDateTime);
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    "Error in {ErrorName} occured during adding job for lesson id={Lessonid}. Error: {ErrorDescription}",
                    ex.Source, lessonId, ex.Message);
            }
        }

        public async Task ChangeLessonData(Guid lessonId, DateTime newStartDateTime)
        {
            if (lessonId == Guid.Empty) return;
            if (newStartDateTime <= DateTime.Now) return;
            foreach (var check in _dbContext.StatusControlItems.Where(
                x => x.LessonWithStateId == lessonId))
                try
                {
                    check.StartDateTime = newStartDateTime;
                    check.Attempts = 0;
                    check.IsProcessed = false;
                    check.IsRejected = false;
                    _logger.LogInformation("Start date for lesson id={Lessonid}  was changed to value {NewStartDate}",
                        check.LessonWithStateId, check.StartDateTime);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during updating job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, check.LessonWithStateId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }

        public async Task RejectJob(Guid lessonId)
        {
            if (lessonId == Guid.Empty) return;
            foreach (var check in _dbContext.StatusControlItems.Where(
                x => x.LessonWithStateId == lessonId && !x.IsRejected))
                try
                {
                    check.IsRejected = true;
                    _logger.LogInformation("job for lesson id={Lessonid}  was rejected at {Date}",
                        check.LessonWithStateId, DateTime.Now);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during rejecting job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, check.LessonWithStateId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteJob(Guid lessonId)
        {
            if (lessonId == Guid.Empty) return;
            foreach (var check in _dbContext.StatusControlItems.Where(
                x => x.LessonWithStateId == lessonId))
                try
                {
                    _dbContext.StatusControlItems.Remove(check);
                    _logger.LogInformation("Start date for lesson id={Lessonid}  was deleted at {Date}",
                        check.LessonWithStateId, DateTime.Now);
                }
                catch (Exception ex)
                {
                    _logger.LogError(
                        "Error in {ErrorName} occured during deleting job for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, check.LessonWithStateId, ex.Message);
                    return;
                }

            await _dbContext.SaveChangesAsync();
        }
    }
}