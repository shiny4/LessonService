using System;
using System.Linq;
using FluentScheduler;
using Lessons.DataAccess;
using Lessons.Entities.Enums;
using Lessons.Infrastructure.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Lessons.Infrastructure.BackgroundJobs.StatusControl
{
    public class CheckStatusJob : IJob
    {
        //private readonly DataContext _dbContext;
        private readonly ILogger<CheckStatusJob> _logger;
        private readonly IStatusControlRepository _statusControlRepository;

        public CheckStatusJob(ILogger<CheckStatusJob> logger, IStatusControlRepository statusControlRepository)
        {
            //_dbContext = dbContext;
            _logger = logger;
            _statusControlRepository = statusControlRepository;
        }

        public async void Execute()
        {
            _logger.LogInformation("CheckStatus job started");
            var list = await _statusControlRepository.GetActiveJobsAsync();
            foreach (var check in list)
            {
                try
                {
                    if (check.LessonWithState.Status == LessonStatus.Canceled ||
                        check.LessonWithState.Status == LessonStatus.Completed ||
                        check.LessonWithState.Status == LessonStatus.Expired)
                    {
                        check.IsRejected = true;
                        await _statusControlRepository.UpdateAsync(check);
                        continue;
                    }

                    check.LastProcessTime = DateTime.Now;
                    var result = check.LessonWithState.CheckStatus();
                    if (result)
                    {
                        check.IsProcessed = true;
                        switch (check.LessonWithState.Status)
                        {
                            case LessonStatus.Expired:
                            case LessonStatus.Completed:
                                check.IsRejected = true;
                                break;
                            case LessonStatus.Pending:
                            case LessonStatus.OpenedForSubscribe:
                                check.Attempts = 0;
                                break;
                        }

                        _logger.LogInformation("Status for lesson id={Lessonid} to value {Status}",
                            check.LessonWithStateId, check.LessonWithState.Status);
                    }
                }
                catch (Exception ex)
                {
                    check.Attempts++;
                    if (check.Attempts >= 10) check.IsRejected = true;
                    _logger.LogError(
                        "Error in {ErrorName} occured during checking status for lesson id={Lessonid}. Error: {ErrorDescription}",
                        ex.Source, check.LessonWithStateId, ex.Message);
                }

                await _statusControlRepository.UpdateAsync(check);
                _logger.LogInformation("CheckStatus job processed");
            }
        }
    }
}