using FluentScheduler;
using Microsoft.Extensions.Configuration;

namespace Lessons.Infrastructure.BackgroundJobs.StatusControl
{
    public class StatusControlSchedulerRegistry : Registry
    {
        private readonly IConfigurationRoot _configuration;

        public StatusControlSchedulerRegistry(IConfigurationRoot configuration)
        {
            _configuration = configuration;
            var interval = configuration.GetValue<int>("CheckStatusJobInterval");
            Schedule<CheckStatusJob>().NonReentrant().ToRunEvery(interval).Minutes();
        }
    }
}