using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using FluentScheduler;
using Lessons.DataAccess;
using Lessons.MassTransitService.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Lessons.Infrastructure.BackgroundJobs.Email
{
    public class SendEmailsJob : IJob
    {
        private readonly DataContext _dbContext;
        private readonly ILogger<SendEmailsJob> _logger;
        private readonly ISendEmailService _sendEmailService;

        public SendEmailsJob(
            DataContext dbContext,
            ILogger<SendEmailsJob> logger,
            ISendEmailService sendEmailService)
        {
            _dbContext = dbContext;
            _logger = logger;
            _sendEmailService = sendEmailService;
        }

        public async void Execute()
        {
            _logger.LogInformation("Email job started");
            var dateTimeNow = DateTime.Now;
            var list = await _dbContext.Emails
                .Where(x => !x.IsSended && x.Attempts < 3 && !x.IsRejected && x.StartDateTime <= dateTimeNow)
                .ToListAsync();
            foreach (var email in list)
                try
                {
                    _sendEmailService.Send(email.Address, email.Subject, email.Body);
                    email.IsSended = true;
                    _logger.LogInformation("Email for {EmailAdress} with subject {EmailSubject} was sent",
                        email.Address, email.Subject);
                }
                catch (Exception ex)
                {
                    email.Attempts++;
                    if (email.Attempts >= 3) email.IsRejected = true;
                    _logger.LogError(
                        "Error in {ErrorName} occured during mailing for adress {EmailAdress} with subject {EmailSubject}. Error: {ErrorDescription}",
                        ex.Source, email.Address, email.Subject, ex.Message);
                }

            await _dbContext.SaveChangesAsync();
            _logger.LogInformation("Email job processed");
        }
    }
}