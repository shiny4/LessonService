using FluentScheduler;
using Microsoft.Extensions.Configuration;

namespace Lessons.Infrastructure.BackgroundJobs.Email
{
    public class EmailSchedulerRegistry : Registry
    {
        private readonly IConfigurationRoot _configuration;

        public EmailSchedulerRegistry(IConfigurationRoot configuration)
        {
            _configuration = configuration;
            var interval = configuration.GetValue<int>("SendEmailJobInterval");
            {
                Schedule<SendEmailsJob>().NonReentrant().ToRunEvery(interval).Minutes();
            }
        }
    }
}