using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Handlers.Students.Commands;
using Lessons.UseCases.Handlers.Students.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lessons.Web.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class StudensController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;

        public StudensController(IMediator mediator, ICurrentUserService currentUserService)
        {
            _mediator = mediator;
            _currentUserService = currentUserService;
        }

        #region Get

        /// <summary>
        /// Получить список учеников, которые присоединились к уроку
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpGet("bylesson/{lessonid:guid}/attendaned")]
        public async Task<ActionResult<List<Guid>>> GetAttendanedStudentsByLessonIdAsync(Guid lessonid)
        {
            return Ok(await _mediator.Send(new GetAttendanedStudents.GetAttendanedStudentsByLessonIdQuery(lessonid)));
        }

        /// <summary>
        /// Получить список учеников, которые записались на урок
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpGet("bylesson/{lessonid:guid}")]
        public async Task<ActionResult<List<Guid>>> GetStudentsByLessonIdAsync(Guid lessonid)
        {
            return Ok(await _mediator.Send(new GetStudents.GetStudentsByLessonIdQuery(lessonid)));
        }

        /// <summary>
        /// Получить ссылку на видеоконференцию урока
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        /// <param name="studentid">ид ученика, присоединившегося к начавшемуся уроку</param>
        [Authorize]
        [HttpGet("{studentid:guid}/{lessonid:guid}/invitelink")]
        public async Task<ActionResult<string>> GetInvitelinkByLessonIdAsync(Guid lessonid, Guid studentid)
        {
            Guid realId;
            if (studentid != Guid.Empty && _currentUserService.Role == "Admin")
            {
                realId = studentid;
            }
            else
            {
                if (_currentUserService.UserId != Guid.Empty)
                    realId = _currentUserService.UserId;
                else return BadRequest();
            }

            return Ok(await _mediator.Send(new GetInvitelink.GetInvitelinkByLessonIdQuery(lessonid, realId)));
        }

        #endregion

        #region Patch

        /// <summary>
        /// Записаться на урок
        /// </summary>
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPatch("subscribe")]
        public async Task<IActionResult> SubscribeStudentByLessonIdAsync(
            SubscribeStudent.SubscribeStudentByLessonIdCommand command)
        {
            if (command.StudentId == Guid.Empty ||
                _currentUserService.Role != "Admin" && _currentUserService.UserId != command.StudentId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Отписаться от урока
        /// </summary>
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPatch("unsubscribe")]
        public async Task<IActionResult> UnSubscribeStudentByLessonIdAsync(
            UnSubscribeStudent.UnSubscribeStudentByLessonIdCommand command)
        {
            if (command.StudentId == Guid.Empty ||
                _currentUserService.Role != "Admin" && _currentUserService.UserId != command.StudentId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Присоединиться к уроку после его начала
        /// </summary>
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpPatch("join")]
        public async Task<IActionResult> JoinStudentByLessonIdAsync(JoinStudent.JoinStudentByLessonIdCommand command)
        {
            if (command.StudentId == Guid.Empty ||
                _currentUserService.Role != "Admin" && _currentUserService.UserId != command.StudentId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        #endregion
    }
}