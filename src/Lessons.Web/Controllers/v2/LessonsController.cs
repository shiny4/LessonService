﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Lessons.Infrastructure.Interfaces.Services;
using Lessons.UseCases.Handlers.Lessons.Commands;
using Lessons.UseCases.Handlers.Lessons.Dto;
using Lessons.UseCases.Handlers.Lessons.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;

namespace Lessons.Web.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LessonsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;


        public LessonsController(IMediator mediator, ICurrentUserService currentUserService)
        {
            _mediator = mediator;
            _currentUserService = currentUserService;
        }

        #region Get

        /// <summary>
        /// Список всех уроков
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<ShortLessonResponseDTO>>> GetAllLessonsAsync()
        {
            return Ok(await _mediator.Send(new GetAll.GetAllQuery()));
        }

        /// <summary>
        /// Информация об одном уроке по его ид 
        /// </summary>
        /// <param name="id">ид урока</param>
        [Authorize]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<LessonResponseDTO>> GetLessonByIdAsync(Guid id)
        {
            return Ok(await _mediator.Send(new GetById.GetByIdQuery(id)));
        }

        /// <summary>
        /// Информация об отмененном уроке по его ид
        /// </summary>
        /// <param name="id">ид урока</param>
        [Authorize]
        [HttpGet("{id:guid}/canceledinfo")]
        public async Task<ActionResult<CanceledLessonResponseDto>> GetCanceledLessonByIdAsync(Guid id)
        {
            return Ok(await _mediator.Send(new GetCanceledLesson.GetCanceledLessonQuery(id)));
        }

        /// <summary>
        /// Список уроков по ид студента
        /// </summary>
        /// <param name="studentid">ид студента</param>
        [Authorize(Roles = "Student, Both,Admin")]
        [HttpGet("bystudent/{studentid:guid}")]
        public async Task<ActionResult<ShortLessonResponseDTO>> GetLessonByStudentIdAsync(Guid studentid)
        {
            Guid realId;
            if (studentid != Guid.Empty && _currentUserService.Role == "Admin")
            {
                realId = studentid;
            }
            else
            {
                if (_currentUserService.UserId != Guid.Empty)
                    realId = _currentUserService.UserId;
                else return BadRequest();
            }

            return Ok(await _mediator.Send(new GetByStudentId.GetByStudentIdQuery(realId)));
        }

        /// <summary>
        /// Список уроков по ид преподавателя
        /// </summary>
        /// <param name="teacherid">ид преподавателя</param>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpGet("byteacher/{teacherid:guid}")]
        public async Task<ActionResult<ShortLessonResponseDTO>> GetLessonByTeacherIdAsync(Guid teacherid)
        {
            Guid realId;
            if (teacherid != Guid.Empty && _currentUserService.Role == "Admin")
            {
                realId = teacherid;
            }
            else
            {
                if (_currentUserService.UserId != Guid.Empty)
                    realId = _currentUserService.UserId;
                else return BadRequest();
            }

            return Ok(await _mediator.Send(new GetByTeacherId.GetByTeacherIdQuery(realId)));
        }

        #endregion

        #region Post

        /// <summary>
        /// Добавить новый урок без заявки
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPost]
        public async Task<IActionResult> CreateUnboundLessonAsync(CreateUnbound.CreateUnboundLessonCommand command)
        {
            if (command.TeacherId == Guid.Empty ||
                _currentUserService.Role != "Admin" && _currentUserService.UserId != command.TeacherId)
                return BadRequest();

            var response = await _mediator.Send(command);
            return Created("", response);
        }

        /// <summary>
        /// Добавить новый урок по заявке
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPost("byrequest")]
        public async Task<IActionResult> CreateLessonbyRequestAsync(
            CreatebyRequest.CreateLessonbyRequestCommand command)
        {
            if (command.TeacherId == Guid.Empty ||
                _currentUserService.Role != "Admin" && _currentUserService.UserId != command.TeacherId)
                return BadRequest();

            var response = await _mediator.Send(command);
            return Created("", response);
        }

        #endregion

        #region Put

        /// <summary>
        /// Изменить урок
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPut]
        public async Task<IActionResult> UpdateLessonAsync(Update.UpdateLessonCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();

            return Ok(await _mediator.Send(command));
        }

        #endregion

        #region Patch

        /// <summary>
        /// Изменить время начала урока
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("starttime")]
        public async Task<IActionResult> MoveLessonTimeAsync(MoveTime.MoveLessonTimeCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();

            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Изменить длительность урока
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("duration")]
        public async Task<IActionResult> MoveLessonEndAsync(MoveEnd.MoveLessonEndCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();

            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Изменить максимальное количество учеников для урока
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("maxstudentcount")]
        public async Task<IActionResult> SetMaxStudentCountAsync(MaxStudentCount.SetMaxStudentCountCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Начать урок
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("start")]
        public async Task<IActionResult> StartLessonAsync(Start.StartLessonCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Закончить урок
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("stop")]
        public async Task<IActionResult> StopLessonAsync(Stop.StopLessonCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Отменить урок
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpPatch("cancel")]
        public async Task<IActionResult> CancelLessonAsync(Cancel.CancelLessonCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        #endregion

        #region Delete

        /// <summary>
        /// Удалить урок
        /// </summary>
        [Authorize(Roles = "Teacher, Both,Admin")]
        [HttpDelete]
        public async Task<IActionResult> DeleteLessonByIdAsync(DeleteById.DeleteLessonByIdCommand command)
        {
            if (command.InitiatorId == Guid.Empty || _currentUserService.Role != "Admin" &&
                _currentUserService.UserId != command.InitiatorId)
                return BadRequest();
            return Ok(await _mediator.Send(command));
        }

        #endregion
    }
}