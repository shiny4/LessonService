using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lessons.UseCases.Handlers.Students.Commands;
using Lessons.UseCases.Handlers.Students.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Lessons.Web.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class StudensController : ControllerBase
    {
        private readonly IMediator _mediator;

        public StudensController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #region Get

        /// <summary>
        /// Получить список учеников, которые присоединились к уроку
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        [HttpGet("bylesson/{lessonid:guid}/attendaned")]
        public async Task<ActionResult<List<Guid>>> GetAttendanedStudentsByLessonIdAsync(Guid lessonid)
        {
            return Ok(await _mediator.Send(new GetAttendanedStudents.GetAttendanedStudentsByLessonIdQuery(lessonid)));
        }

        /// <summary>
        /// Получить список учеников, которые записались на урок
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        [HttpGet("bylesson/{lessonid:guid}")]
        public async Task<ActionResult<List<Guid>>> GetStudentsByLessonIdAsync(Guid lessonid)
        {
            return Ok(await _mediator.Send(new GetStudents.GetStudentsByLessonIdQuery(lessonid)));
        }

        /// <summary>
        /// Получить ссылку на видеоконференцию урока
        /// </summary>
        /// <param name="lessonid">ид урока</param>
        /// <param name="studentid">ид ученика, присоединившегося к начавшемуся уроку</param>
        [HttpGet("{studentid:guid}/{lessonid:guid}/invitelink")]
        public async Task<ActionResult<string>> GetInvitelinkByLessonIdAsync(Guid lessonid, Guid studentid)
        {
            return Ok(await _mediator.Send(new GetInvitelink.GetInvitelinkByLessonIdQuery(lessonid, studentid)));
        }

        #endregion

        #region Patch

        /// <summary>
        /// Записаться на урок
        /// </summary>
        [HttpPatch("subscribe")]
        public async Task<IActionResult> SubscribeStudentByLessonIdAsync(
            SubscribeStudent.SubscribeStudentByLessonIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Отписаться от урока
        /// </summary>
        [HttpPatch("unsubscribe")]
        public async Task<IActionResult> UnSubscribeStudentByLessonIdAsync(
            UnSubscribeStudent.UnSubscribeStudentByLessonIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Присоединиться к уроку после его начала
        /// </summary>
        [HttpPatch("join")]
        public async Task<IActionResult> JoinStudentByLessonIdAsync(JoinStudent.JoinStudentByLessonIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        #endregion
    }
}