﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lessons.UseCases.Handlers.Lessons.Commands;
using Lessons.UseCases.Handlers.Lessons.Dto;
using Lessons.UseCases.Handlers.Lessons.Queries;
using MediatR;

namespace Lessons.Web.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LessonsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public LessonsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #region Get

        /// <summary>
        /// Список всех уроков
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<ShortLessonResponseDTO>>> GetAllLessonsAsync()
        {
            return Ok(await _mediator.Send(new GetAll.GetAllQuery()));
        }

        /// <summary>
        /// Информация об одном уроке по его ид 
        /// </summary>
        /// <param name="id">ид урока</param>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<LessonResponseDTO>> GetLessonByIdAsync(Guid id)
        {
            return Ok(await _mediator.Send(new GetById.GetByIdQuery(id)));
        }

        /// <summary>
        /// Информация об отмененном уроке по его ид
        /// </summary>
        /// <param name="id">ид урока</param>
        [HttpGet("{id:guid}/canceledinfo")]
        public async Task<ActionResult<CanceledLessonResponseDto>> GetCanceledLessonByIdAsync(Guid id)
        {
            return Ok(await _mediator.Send(new GetCanceledLesson.GetCanceledLessonQuery(id)));
        }

        /// <summary>
        /// Список уроков по ид студента
        /// </summary>
        /// <param name="studentid">ид студента</param>
        [HttpGet("bystudent/{studentid:guid}")]
        public async Task<ActionResult<ShortLessonResponseDTO>> GetLessonByStudentIdAsync(Guid studentid)
        {
            //var u=User.Claims
            return Ok(await _mediator.Send(new GetByStudentId.GetByStudentIdQuery(studentid)));
        }

        /// <summary>
        /// Список уроков по ид преподавателя
        /// </summary>
        /// <param name="teacherid">ид преподавателя</param>
        [HttpGet("byteacher/{teacherid:guid}")]
        public async Task<ActionResult<ShortLessonResponseDTO>> GetLessonByTeacherIdAsync(Guid teacherid)
        {
            return Ok(await _mediator.Send(new GetByTeacherId.GetByTeacherIdQuery(teacherid)));
        }

        #endregion

        #region Post

        /// <summary>
        /// Добавить новый урок без заявки
        /// </summary>
        [HttpPost]
        public async Task<CreatedResult> CreateUnboundLessonAsync(CreateUnbound.CreateUnboundLessonCommand command)
        {
            var response = await _mediator.Send(command);
            return Created("", response);
        }

        /// <summary>
        /// Добавить новый урок по заявке
        /// </summary>
        [HttpPost("byrequest")]
        public async Task<CreatedResult> CreateLessonbyRequestAsync(
            CreatebyRequest.CreateLessonbyRequestCommand command)
        {
            //return Ok(await _mediator.Send(command));
            var response = await _mediator.Send(command);
            return Created("", response);
        }

        #endregion

        #region Put

        /// <summary>
        /// Изменить урок
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> UpdateLessonAsync(Update.UpdateLessonCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        #endregion

        #region Patch

        /// <summary>
        /// Изменить время начала урока
        /// </summary>
        [HttpPatch("starttime")]
        public async Task<IActionResult> MoveLessonTimeAsync(MoveTime.MoveLessonTimeCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Изменить длительность урока
        /// </summary>
        [HttpPatch("duration")]
        public async Task<IActionResult> MoveLessonEndAsync(MoveEnd.MoveLessonEndCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Изменить максимальное количество учеников для урока
        /// </summary>
        [HttpPatch("maxstudentcount")]
        public async Task<IActionResult> SetMaxStudentCountAsync(MaxStudentCount.SetMaxStudentCountCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Начать урок
        /// </summary>
        [HttpPatch("start")]
        public async Task<IActionResult> StartLessonAsync(Start.StartLessonCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Закончить урок
        /// </summary>
        [HttpPatch("stop")]
        public async Task<IActionResult> StopLessonAsync(Stop.StopLessonCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        /// <summary>
        /// Отменить урок
        /// </summary>
        [HttpPatch("cancel")]
        public async Task<IActionResult> CancelLessonAsync(Cancel.CancelLessonCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        #endregion

        #region Delete

        /// <summary>
        /// Удалить урок
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteLessonByIdAsync(DeleteById.DeleteLessonByIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        #endregion
    }
}