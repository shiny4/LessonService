using System;
using Autofac.Extensions.DependencyInjection;
using FluentScheduler;
using Lessons.DataAccess;
using Lessons.Infrastructure.BackgroundJobs;
using Lessons.Infrastructure.BackgroundJobs.Email;
using Lessons.Infrastructure.BackgroundJobs.StatusControl;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Lessons.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Read Configuration from appSettings
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            //Initialize Logger
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            try
            {
                Log.Information("Application Starting");
                var host = CreateHostBuilder(args).Build();
                if (config.GetValue<bool>("migrateDB") == true)
                {
                    using var scope = host.Services.CreateScope();
                    var db = scope.ServiceProvider.GetRequiredService<DataContext>();
                    db.Database.EnsureDeleted();
                    db.Database.Migrate();
                }

                JobManager.JobFactory = new JobFactory(host.Services);
                JobManager.Initialize(
                    new EmailSchedulerRegistry(config),
                    new StatusControlSchedulerRegistry(config));
                Log.Information("Email service started");
                Log.Information("CheckStatus service started");

                JobManager.JobException += info =>
                {
                    var logger = host.Services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(info.Exception, "Unhandled exceptionin job {info.Name} ");
                };

                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The Application failed to start");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .UseSerilog() //Uses Serilog instead of default .NET Logger
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}