using Lessons.DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Lessons.Infrastructure.BackgroundJobs.Email;
using Lessons.Infrastructure.BackgroundJobs.StatusControl;
using Lessons.MassTransitService;
using Lessons.UseCases;
using Lessons.Web.Options;
using Lessons.Web.Utils.Extensions;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Serilog;

namespace Lessons.Web
{
    public class Startup
    {
        public ILifetimeScope AutofacContainer { get; private set; }
        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        private JWTOptions _jwtOptions;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _jwtOptions = Configuration
                .GetSection("JWTOptions")
                .Get<JWTOptions>();
        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacConfigurationModule());
        }
        public IConfiguration Configuration { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<JWTOptions>(Configuration.GetSection("JWTOptions"));
            services.AddUseCaseLayer();
            services.AddDataAccess(Configuration);

            services.AddJWTAuthentication(_jwtOptions);

            services.AddTransient<SendEmailsJob>();
            services.AddTransient<CheckStatusJob>();

            services.AddHttpContextAccessor();

            services.AddGraphQL();
            services.AddMassTransitService(Configuration);
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder
                            .AllowAnyHeader()
                            .AllowAnyOrigin()
                            //.WithOrigins("http://localhost:3000")
                            .WithMethods("POST", "GET", "PATCH", "PUT", "DELETE");
                        //.AllowCredentials();
                    });
            });
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddApiVersioningExtension();
            services.AddSwagger();

            services.AddHealthChecks();
        }

        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            DatabaseInitializer dbInitializer,
            IConfiguration configuration,
            IApiVersionDescriptionProvider provider)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            app.UseSerilogRequestLogging();
            app.UseExceptionHandler(env);

            //if (env.IsProduction())
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();

            if (configuration.GetValue<bool>("recreateDB") == true) dbInitializer.Refresh();
            if (configuration.GetValue<bool>("InitializeDB") == true) dbInitializer.Initialize();

            app.UseSwaggerExtension(provider);

            app.UseHttpsRedirection();

            app.UseHealthChecks("/health");

            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseGraphQlExtension(env);

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}