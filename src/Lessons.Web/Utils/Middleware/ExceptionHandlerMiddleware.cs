using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Lessons.Entities.Exceptions;
using Lessons.UseCases.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;

namespace Lessons.Web.Utils.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<Program> _logger;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<Program> logger)
        {
            _next = next;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var response = FindException(ex);
                if (response.Status != null)
                {
                    context.Response.StatusCode = response.Status.Value;
                    context.Response.ContentType = "application/json";
                }

                await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
            }
        }

        private ExceptionResponse FindException(Exception ex)
        {
            if (ex is AppException appException)
            {
                _logger.LogWarning("Error in {source} : {message}", appException.Source, appException.Message);
                return new ExceptionResponse
                {
                    Content = appException.Message,
                    Status = appException.StatusCode
                };
            }
            else if (ex is LessonValidationException validationExceptionException)
            {
                _logger.LogWarning("Validation failed for {Request}. Error: {Error}",
                    validationExceptionException.RequestName, validationExceptionException.Errors);
                return new ValidationExceptionResponse
                {
                    Content = validationExceptionException.Message,
                    Status = validationExceptionException.StatusCode,
                    Errors = validationExceptionException.Errors
                };
            }

            if (ex is LessonParameterException lessonParameterException)
            {
                _logger.LogWarning("Validation failed for {Property}. Error: {Error}",
                    lessonParameterException.PropertyName, lessonParameterException.ErrorMessage);
                return new ValidationExceptionResponse
                {
                    Content = lessonParameterException.Message,
                    Status = 400,
                    Errors = new List<LessonValidationException.ErrorValue>()
                    {
                        new(lessonParameterException.PropertyName,
                            lessonParameterException.ErrorMessage)
                    }
                };
            }
            else if (ex is LessonException lessonException)
            {
                _logger.LogWarning("Error in {source} : {message}", lessonException.Source, lessonException.Message);
                return new ExceptionResponse
                {
                    Content = lessonException.Message,
                    Status = 405
                };
            }
            else
            {
                _logger.LogError("Error in {source}: {message}", ex.Source, ex.Message);
                return new ExceptionResponse
                {
                    Content = ex.Message,
                    Status = (int) HttpStatusCode.InternalServerError
                };
            }
        }

        public class ExceptionResponse
        {
            public int? Status { get; set; }
            public string Content { get; set; }
        }

        public class ValidationExceptionResponse : ExceptionResponse
        {
            public List<LessonValidationException.ErrorValue> Errors { get; set; }
        }
    }
}