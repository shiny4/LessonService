using Autofac;
using Lessons.DataAccess.Repositories;
using Lessons.Infrastructure.Services;
using Lessons.MassTransitService.Services;

namespace Lessons.Web.Utils.Extensions
{
    public class AutofacConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var repositories = typeof(BaseRepository<>).Assembly;
            builder.RegisterAssemblyTypes(repositories)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(EmailService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(SendEmailService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}