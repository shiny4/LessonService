using Lessons.Web.Utils.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Hosting;

namespace Lessons.Web.Utils.Extensions
{
    public static class AppExtensions
    {
        public static IApplicationBuilder UseSwaggerExtension(this IApplicationBuilder builder,
            IApiVersionDescriptionProvider provider)
        {
            builder.UseSwagger();
            builder.UseSwaggerUI(opt =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                    opt.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());
            });
            return builder;
        }

        public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder builder, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                return builder.UseDeveloperExceptionPage();
            else
                return builder.UseMiddleware<ExceptionHandlerMiddleware>();
        }

        public static IApplicationBuilder UseGraphQlExtension(this IApplicationBuilder builder, IWebHostEnvironment env)
        {
            builder.UseEndpoints(x => x.MapGraphQL());
            return builder;
        }
    }
}