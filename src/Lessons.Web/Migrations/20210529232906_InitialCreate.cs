﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lessons.Web.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Emails",
                table => new
                {
                    Id = table.Column<Guid>("uuid", nullable: false),
                    LessonWithStateIdId = table.Column<Guid>("uuid", nullable: false),
                    StartDateTime = table.Column<DateTime>("timestamp without time zone", nullable: false),
                    Address = table.Column<string>("text", nullable: true),
                    Subject = table.Column<string>("text", nullable: true),
                    Body = table.Column<string>("text", nullable: true),
                    IsSended = table.Column<bool>("boolean", nullable: false),
                    IsRejected = table.Column<bool>("boolean", nullable: false),
                    Attempts = table.Column<int>("integer", nullable: false),
                    Category = table.Column<int>("integer", nullable: false),
                    Created = table.Column<DateTime>("timestamp without time zone", nullable: false)
                },
                constraints: table => { table.PrimaryKey("PK_Emails", x => x.Id); });

            migrationBuilder.CreateTable(
                "Lessons",
                table => new
                {
                    Id = table.Column<Guid>("uuid", nullable: false),
                    InviteLink = table.Column<string>("text", nullable: true),
                    Created = table.Column<DateTime>("timestamp without time zone", nullable: false),
                    Status = table.Column<int>("integer", nullable: false),
                    LessonTitle = table.Column<string>("text", nullable: false),
                    LessonDescription =
                        table.Column<string>("character varying(400)", maxLength: 400, nullable: true),
                    StartDate = table.Column<DateTime>("timestamp without time zone", nullable: false),
                    DurationInMin = table.Column<int>("integer", nullable: false),
                    Price = table.Column<decimal>("numeric", nullable: false),
                    MaxStudentCount = table.Column<int>("integer", nullable: false),
                    MinStudentCount = table.Column<int>("integer", nullable: false),
                    ParentRequestId = table.Column<Guid>("uuid", nullable: false),
                    TeacherId = table.Column<Guid>("uuid", nullable: false),
                    CanceledLessonId = table.Column<Guid>("uuid", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Lessons", x => x.Id); });

            migrationBuilder.CreateTable(
                "CanceledLessons",
                table => new
                {
                    Id = table.Column<Guid>("uuid", nullable: false),
                    LessonId = table.Column<Guid>("uuid", nullable: false),
                    InitiatorId = table.Column<Guid>("uuid", nullable: false),
                    CancelDate = table.Column<DateTime>("timestamp without time zone", nullable: false),
                    Reason = table.Column<string>("text", nullable: true),
                    Created = table.Column<DateTime>("timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CanceledLessons", x => x.Id);
                    table.ForeignKey(
                        "FK_CanceledLessons_Lessons_LessonId",
                        x => x.LessonId,
                        "Lessons",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "StatusControlItems",
                table => new
                {
                    Id = table.Column<Guid>("uuid", nullable: false),
                    LessonWithStateId = table.Column<Guid>("uuid", nullable: false),
                    StartDateTime = table.Column<DateTime>("timestamp without time zone", nullable: false),
                    IsProcessed = table.Column<bool>("boolean", nullable: false),
                    IsRejected = table.Column<bool>("boolean", nullable: false),
                    LastProcessTime = table.Column<DateTime>("timestamp without time zone", nullable: true),
                    Attempts = table.Column<int>("integer", nullable: false),
                    Created = table.Column<DateTime>("timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusControlItems", x => x.Id);
                    table.ForeignKey(
                        "FK_StatusControlItems_Lessons_LessonWithStateId",
                        x => x.LessonWithStateId,
                        "Lessons",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Students",
                table => new
                {
                    Id = table.Column<Guid>("uuid", nullable: false),
                    PublicId = table.Column<Guid>("uuid", nullable: false),
                    IsAttendaned = table.Column<bool>("boolean", nullable: false),
                    LessonId = table.Column<Guid>("uuid", nullable: false),
                    IsPaid = table.Column<bool>("boolean", nullable: false),
                    Created = table.Column<DateTime>("timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        "FK_Students_Lessons_LessonId",
                        x => x.LessonId,
                        "Lessons",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_CanceledLessons_LessonId",
                "CanceledLessons",
                "LessonId",
                unique: true);

            migrationBuilder.CreateIndex(
                "IX_StatusControlItems_LessonWithStateId",
                "StatusControlItems",
                "LessonWithStateId");

            migrationBuilder.CreateIndex(
                "IX_Students_LessonId",
                "Students",
                "LessonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "CanceledLessons");

            migrationBuilder.DropTable(
                "Emails");

            migrationBuilder.DropTable(
                "StatusControlItems");

            migrationBuilder.DropTable(
                "Students");

            migrationBuilder.DropTable(
                "Lessons");
        }
    }
}