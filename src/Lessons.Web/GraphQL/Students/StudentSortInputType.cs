using HotChocolate.Data.Sorting;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.Students
{
    public class StudentSortInputType : SortInputType<Student>
    {
        protected override void Configure(ISortInputTypeDescriptor<Student> descriptor)
        {
            descriptor.BindFieldsExplicitly();
            descriptor.Field(f => f.Id);
            descriptor.Field(f => f.IsAttendaned);
            descriptor.Field(f => f.IsPaid);
            descriptor.Field(f => f.PublicId);
        }
    }
}