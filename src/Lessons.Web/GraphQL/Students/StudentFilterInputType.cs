using HotChocolate.Data.Filters;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.Students
{
    public class StudentFilterInputType : FilterInputType<Student>
    {
        protected override void Configure(IFilterInputTypeDescriptor<Student> descriptor)
        {
            descriptor.Ignore(t => t.Created);
            descriptor.Ignore(t => t.Lesson);
            descriptor.Ignore(t => t.Id);
        }
    }
}