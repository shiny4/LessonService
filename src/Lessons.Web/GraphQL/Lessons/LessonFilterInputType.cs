using HotChocolate.Data.Filters;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.Lessons
{
    public class LessonFilterInputType : FilterInputType<LessonWithState>
    {
        protected override void Configure(IFilterInputTypeDescriptor<LessonWithState> descriptor)
        {
            descriptor.Ignore(t => t.Created);
            descriptor.Ignore(t => t.CanceledLessonId);
            descriptor.Ignore(t => t.Students);
            descriptor.Ignore(t => t.ByRequest);
            descriptor.Ignore(t => t.EndDate);
            descriptor.Ignore(t => t.StudentsCount);
            descriptor.Ignore(t => t.СanceledLesson);
            descriptor.Ignore(t => t.AttendanedStudentIds);
            descriptor.Ignore(t => t.DurationInMin);
            descriptor.Ignore(t => t.MaxStudentCount);
            descriptor.Ignore(t => t.MinStudentCount);
            descriptor.Ignore(t => t.SubscribedStudentIds);
        }
    }
}