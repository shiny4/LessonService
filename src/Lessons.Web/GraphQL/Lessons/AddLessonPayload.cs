using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.Lessons
{
    public class AddLessonWithStatePayload
    {
        public AddLessonWithStatePayload(LessonWithState lesson)
        {
            LessonWithState = lesson;
        }

        public LessonWithState LessonWithState { get; }
    }
}