using HotChocolate.Data.Sorting;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.Lessons
{
    public class LessonSortInputType : SortInputType<LessonWithState>
    {
        protected override void Configure(ISortInputTypeDescriptor<LessonWithState> descriptor)
        {
            descriptor.BindFieldsExplicitly();
            descriptor.Field(f => f.Id);
            descriptor.Field(f => f.Price);
            descriptor.Field(f => f.Status);
            descriptor.Field(f => f.LessonTitle);
            descriptor.Field(f => f.StartDate);
            descriptor.Field(f => f.TeacherId);
            descriptor.Field(f => f.ParentRequestId);
        }
    }
}