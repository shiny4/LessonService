using System;

namespace Lessons.Web.GraphQL.Lessons
{
    public record AddLessonWithStateInput(
        Guid TeacherId,
        string LessonTitle,
        string LessonDescription,
        DateTime StartDate,
        int DurationInMin,
        int MaxStudentCount,
        int MinStudentCount,
        decimal Price);
}