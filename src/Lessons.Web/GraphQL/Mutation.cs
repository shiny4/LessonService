using System.Threading.Tasks;
using HotChocolate;
using Lessons.DataAccess;
using Lessons.Entities.Models;
using Lessons.Web.GraphQL.Lessons;

namespace Lessons.Web.GraphQL
{
    public class Mutation
    {
        public async Task<AddLessonWithStatePayload> AddLessonWithStateAsync(
            AddLessonWithStateInput input,
            [Service] DataContext context)
        {
            var lesson = LessonWithState.CreateUnboundLesson(
                input.TeacherId,
                input.LessonTitle,
                input.StartDate,
                input.DurationInMin,
                input.LessonDescription,
                input.MaxStudentCount,
                input.MinStudentCount,
                input.Price
            );
            await context.Lessons.AddAsync(lesson);
            await context.SaveChangesAsync();

            return new AddLessonWithStatePayload(lesson);
        }
    }
}