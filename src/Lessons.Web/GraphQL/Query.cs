using System;
using System.Linq;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using Lessons.DataAccess;
using Lessons.Entities.Models;
using Lessons.Web.GraphQL.CanceledLessons;
using Lessons.Web.GraphQL.Lessons;
using Lessons.Web.GraphQL.Students;
using Microsoft.EntityFrameworkCore;

namespace Lessons.Web.GraphQL
{
    public class Query
    {
        [UseFiltering(typeof(LessonFilterInputType))]
        [UseSorting(typeof(LessonSortInputType))]
        //[UsePaging(typeof(LessonWithState),MaxPageSize = 5, IncludeTotalCount = true)]
        public IQueryable<LessonWithState> GetLessons([Service] DataContext context)
        {
            return context.Lessons;
        }

        [UseFiltering(typeof(LessonFilterInputType))]
        [UseSorting(typeof(LessonSortInputType))]
        public IQueryable<LessonWithState> GetLessonsForStudent([Service] DataContext context, Guid studentid)
        {
            return context.Lessons.Where(e => e.Students.Any(c => c.PublicId == studentid));
        }

        [UseFiltering(typeof(StudentFilterInputType))]
        [UseSorting(typeof(StudentSortInputType))]
        public IQueryable<Student> GetStudents([Service] DataContext context)
        {
            return context.Students;
        }

        // [UseFiltering(typeof(CanceledLessonFilterInputType))]
        // [UseSorting(typeof(CanceledLessonSortInputType))]
        public IQueryable<CanceledLesson> GetCanceledLessons([Service] DataContext context)
        {
            return context.CanceledLessons;
        }
    }
}