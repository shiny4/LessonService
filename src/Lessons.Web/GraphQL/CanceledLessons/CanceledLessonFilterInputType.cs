using HotChocolate.Data.Filters;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.CanceledLessons
{
    public class CanceledLessonFilterInputType : FilterInputType<CanceledLesson>
    {
        protected override void Configure(IFilterInputTypeDescriptor<CanceledLesson> descriptor)
        {
            descriptor.Ignore(t => t.Created);
            descriptor.Ignore(t => t.Lesson);
            descriptor.Ignore(t => t.Id);
        }
    }
}