using HotChocolate.Data.Sorting;
using Lessons.Entities.Models;

namespace Lessons.Web.GraphQL.CanceledLessons
{
    public class CanceledLessonSortInputType : SortInputType<CanceledLesson>
    {
        protected override void Configure(ISortInputTypeDescriptor<CanceledLesson> descriptor)
        {
            descriptor.BindFieldsExplicitly();
            descriptor.Field(f => f.Id);
            descriptor.Field(f => f.CancelDate);
            descriptor.Field(f => f.InitiatorId);
            descriptor.Field(f => f.LessonId);
        }
    }
}