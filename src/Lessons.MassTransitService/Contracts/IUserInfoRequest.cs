using System;

namespace Jumping.Contracts
{
    public interface IUserInfoRequest
    {
        public Guid id { get; set; }
    }
}