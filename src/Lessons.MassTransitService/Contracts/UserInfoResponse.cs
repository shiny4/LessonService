﻿using System;

namespace Jumping.Contracts
{
    public class UserInfoResponse : IUserInfoResponse
    {
        public Guid id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}