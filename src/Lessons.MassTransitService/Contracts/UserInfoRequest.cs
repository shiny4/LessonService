using System;

namespace Jumping.Contracts
{
    public class UserInfoRequest : IUserInfoRequest
    {
        public Guid id { get; set; }
    }
}