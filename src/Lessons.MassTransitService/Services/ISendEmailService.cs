namespace Lessons.MassTransitService.Services
{
    public interface ISendEmailService
    {
        void Send(string address, string subject, string body);
    }
}