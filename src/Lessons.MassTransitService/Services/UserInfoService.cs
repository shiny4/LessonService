using System;
using System.Threading.Tasks;
using Jumping.Contracts;
using Lessons.Infrastructure.Interfaces.Services;
using MassTransit;

namespace Lessons.MassTransitService.Services
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IRequestClient<UserInfoRequest> _requestClient;

        public UserInfoService(ICurrentUserService currentUser, IRequestClient<UserInfoRequest> requestClient)
        {
            _requestClient = requestClient;
        }

        public async Task<UserInfoResponse> Get(Guid userId)
        {
            var request = new UserInfoRequest
            {
                id = userId
            };

            var data = await _requestClient.GetResponse<UserInfoResponse>(request);
            return data.Message;
        }
    }
}