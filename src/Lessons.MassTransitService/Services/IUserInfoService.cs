using System;
using System.Threading.Tasks;
using Jumping.Contracts;

namespace Lessons.MassTransitService.Services
{
    public interface IUserInfoService
    {
        Task<UserInfoResponse> Get(Guid userId);
    }
}