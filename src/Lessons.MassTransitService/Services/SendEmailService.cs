using Jumping.Contracts;
using Lessons.Infrastructure.Interfaces.Services;
using MassTransit;

namespace Lessons.MassTransitService.Services
{
    public class SendEmailService : ISendEmailService
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public SendEmailService(ICurrentUserService currentUser, IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async void Send(string address, string subject, string body)
        {
            await _publishEndpoint.Publish(new EmailNotification
            {
                Address = address,
                Subject = subject,
                Body = body
            });
        }
    }
}