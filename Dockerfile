FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
ENV ASPNETCORE_URLS http://*:8008

WORKDIR /app
COPY ./src/ /app/src/
COPY ./tests/ /app/tests/
COPY "./Lessons.sln" ./

RUN dotnet restore "Lessons.sln"
RUN dotnet build "Lessons.sln" -c Release -o /app/build

WORKDIR /app/src/Lessons.Web
RUN dotnet publish -c release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
EXPOSE 8007
EXPOSE 8008

ENV ASPNETCORE_URLS http://*:8008
ENV TZ=Europe/Moscow

COPY --from=build /app/publish ./
CMD dotnet Lessons.Web.dll